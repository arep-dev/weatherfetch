
# WeatherFetch Application

## Overview

WeatherFetch is a GUI-based application designed for fast and efficient handling of climatic data. It allows users to select and download climate data for specific geographic areas and time periods. This tool is particularly useful for users needing climatic data in formats like WRF or EPW, with more database options planned for future updates. Currently, it supports data from the ERA database.

## Installation

Before running WeatherFetch, ensure you have Python installed on your system. WeatherFetch requires Python 3.x.

1. **Clone the repository or download the source code.**
   ```sh
   git clone https://gitlab.com/arep-dev/weatherfetch.git
   cd WeatherFetch
   ```

2. **Install the required dependencies.**
   A `requirements.txt` file is provided. Install the dependencies using pip:
   ```sh
   pip install -r requirements.txt
   ```

3. **Configure the .cdsapirc file.**
   WeatherFetch requires a `.cdsapirc` file for accessing climate data. Create this file in your home directory with the following content:
   ```plaintext
   url: https://cds.climate.copernicus.eu/api/v2
   key: [your-key-token]
   ```
   Replace `[your-key-token]` with your actual API key token. For information on obtaining a token, visit [CDS API key registration](https://cds.climate.copernicus.eu/api-how-to).

## Running the Application

To run WeatherFetch, execute the following command from the application's root directory:

```sh
sh weatherfetch.sh
```

This script will start the application, opening the first GUI window.

## Using WeatherFetch

Upon launching, you will be greeted with a window where you can select between the Preprocessor and the Client for downloading climatic data.

### Preprocessor

The Preprocessor is designed to help you specify:

- **Geographic Area**: Choose a specific area either by marking a point or defining a region.
- **Data Selection**: Select the specific climatic data you wish to download.
- **Temporal Period**: Define the time frame for which you want the data.
- **Output Format**: Choose the desired format for the downloaded data, such as WRF or EPW.

### Client for Downloading Climatic Data

This option allows you to directly download climatic data from available databases, currently limited to ERA.

- **Specify Output Path**: Users can define the path where the downloaded data will be saved.
- **Data Subsampling**: The tool allows you to split the downloaded data into smaller time periods (e.g., years, semesters, months). This is particularly useful for avoiding HTTP errors from the CDS server due to large data requests.


**Note:** This README is intended for quick start and basic usage. For detailed descriptions of the application's functionalities and code, please refer to the accompanying `documentation.md`.
