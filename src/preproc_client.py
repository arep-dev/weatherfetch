import sys
import os
import json
from tkinter import ttk
import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import datetime
import configparser
from tkcalendar import DateEntry
import customtkinter
from constants import *
from widgets import TimeSelector, TimeStepSelector, DataPopup, LevelPopup
from utilities import convert_to_datetime
# import TkinterMapView
current_directory = os.path.dirname(os.path.realpath(__file__))
tkinter_mapview_path = os.path.join(current_directory, "TkinterMapView")
sys.path.append(tkinter_mapview_path)
from tkintermapview import *

#disable input methods to improve speed
os.environ['XMODIFIERS'] = "@im=none"

class Preprocessor(customtkinter.CTk):
    """
    Main application class for the WeatherFetch application.

    Attributes:
        marker_list (list): Stores references to markers on the map.
        area_list (list): Stores references to areas on the map.
        selected_levels (list): Holds the list of selected levels.
        selected_data (dict): Contains selected data categorized by type.
    """

    def __init__(self, *args, **kwargs):
        """
        Initialize the main application window and its widgets.
        """
        super().__init__(*args, **kwargs)

        self.title(APP_NAME)
        self.geometry(f"{APP_WIDTH}x{APP_HEIGHT}")
        self.minsize(APP_WIDTH, APP_HEIGHT)

        self.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.bind("<Command-q>", self.on_closing)
        self.bind("<Command-w>", self.on_closing)
        self.createcommand('tk::mac::Quit', self.on_closing)

        self.marker_list = []
        self.area_list = []
        self.wrf_var = tk.IntVar(value=1)
        self.epw_var = tk.IntVar()

        # Grid configuration
        self.grid_columnconfigure(0, weight=0)
        self.grid_columnconfigure(1, weight=1)
        self.grid_rowconfigure(0, weight=1)

        self.selected_data   = json.load(open(DATA_LIST_PATH, 'r'))
        self.selected_levels = json.load(open(DEFAULT_LEVELS_PATH,'r'))
        self.source = DEFAULT_DATA_SOURCE
        self.output = DEFAULT_OUTPUT

        # Left frame configuration
        self.frame_left = customtkinter.CTkFrame(master=self, corner_radius=0)
        self.frame_left.grid(row=0, column=0, padx=0, pady=0, sticky="nsew")

        # Right frame configuration
        self.frame_right = customtkinter.CTkFrame(master=self, corner_radius=0)
        self.frame_right.grid(row=0, column=1, rowspan=1, pady=0, padx=0, sticky="nsew")

        # Configure grid row weights and paddings for left frame
        for i in range(12):
            self.frame_left.grid_rowconfigure(i, weight=0, pad=10)

        # Initialize and place widgets
        self.initialize_widgets()

    def initialize_widgets(self):
        """
        Initialize and place all widgets in the application.
        """
        # Notebook widget
        self.tabs = ttk.Notebook(self.frame_left)
        self.tabs.grid(row=3, column=0, columnspan=2, padx=10, pady=10, sticky="nsew")
        self.time_tab = tk.Frame(self.tabs)
        self.data_tab = tk.Frame(self.tabs)
        self.tabs.add(self.data_tab, text="Data")
        self.tabs.add(self.time_tab, text="Time")
        self.tabs.bind("<<NotebookTabChanged>>", self.on_tab_change)

        # Configure time_tab and data_tab
        self.configure_tabs()

        # ComboBox for selecting between Marker and Area
        self.selection_combobox = customtkinter.CTkComboBox(master=self.frame_left, values=["Marker", "Area"])
        self.selection_combobox.grid(row=0, column=0, columnspan=2, padx=10, pady=(7, 0), sticky="ew")
        self.selection_combobox.set("Marker")
        self.selection_combobox.configure(state="readonly") 

        # Set and Clear buttons
        self.set_button = customtkinter.CTkButton(master=self.frame_left, text="Set", command=self.set_event)
        self.clear_button = customtkinter.CTkButton(master=self.frame_left, text="Clear", command=self.clear_event)
        self.set_button.grid(row=1, column=0, padx=10, pady=(0, 10), sticky="w")
        self.clear_button.grid(row=1, column=1, padx=10, pady=(0, 10), sticky="e")

        # Bind resize event
        self.bind("<Configure>", self.on_resize)

        # Map label and option menu
        self.initialize_map_options()

        # Initialize time tab widgets
        self.initialize_time_tab_widgets()

        # Initialize data tab widgets
        self.initialize_data_tab_widgets()

        # Right frame map widget and controls
        self.initialize_map_widget()

    def configure_tabs(self):
        """
        Configure the grid layout for time_tab and data_tab.
        """
        for i in range(4):
            self.time_tab.grid_columnconfigure(i, weight=1)
            self.data_tab.grid_columnconfigure(i, weight=1)
            self.time_tab.grid_rowconfigure(i, weight=1)
            self.data_tab.grid_rowconfigure(i, weight=1)

    def initialize_map_widget(self):
        """
        Initialize the map widget and associated controls in the right frame.
        """
        self.map_widget = TkinterMapView(self.frame_right, corner_radius=0)
        self.frame_right.grid_rowconfigure(1, weight=1)
        self.frame_right.grid_rowconfigure(0, weight=0)
        self.frame_right.grid_columnconfigure(0, weight=1)
        self.frame_right.grid_columnconfigure(1, weight=0)
        self.frame_right.grid_columnconfigure(2, weight=1)
        self.map_widget.grid(row=1, rowspan=1, column=0, columnspan=3, sticky="nswe", padx=(0, 0), pady=(0, 0))
        self.entry = customtkinter.CTkEntry(master=self.frame_right, placeholder_text="type address")
        self.entry.grid(row=0, column=0, sticky="we", padx=(12, 0), pady=12)
        self.entry.bind("<Return>", self.search_event)
        self.button_search = customtkinter.CTkButton(master=self.frame_right, text="Search", width=90,
                                                     command=self.search_event)
        self.button_search.grid(row=0, column=1, sticky="w", padx=(12, 0), pady=12)
        self.button_export = customtkinter.CTkButton(master=self.frame_right, text="export", width=90, command=self.export_data)
        self.button_export.grid(row=0, column=2, sticky="w", padx=(12, 0), pady=12)
        self.map_widget.set_address(DEFAULT_ADRESS)
        self.map_widget.set_zoom(DEFAULT_ZOOM_LEVEL)

    def initialize_map_options(self):
        """
        Initialize map-related widgets in the left frame.
        """
        self.map_label = customtkinter.CTkLabel(self.frame_left, text="Tile Server:", anchor="w")
        self.map_label.grid(row=8, column=0, columnspan=1, padx=(10, 10), pady=(10, 0), sticky="w")
        self.map_option_menu = customtkinter.CTkOptionMenu(self.frame_left,
                                                           values=["OpenStreetMap", "Google normal", "Google satellite"],
                                                           command=self.change_map)
        self.map_option_menu.grid(row=9, column=0, columnspan=2, padx=(10, 10), pady=(0, 10), sticky="ew")
        self.appearance_mode_label = customtkinter.CTkLabel(self.frame_left, text="Appearance Mode:", anchor="w")
        self.appearance_mode_label.grid(row=10, column=0, columnspan=1, padx=(10, 10), pady=(10, 0), sticky="w")
        self.appearance_mode_optionemenu = customtkinter.CTkOptionMenu(self.frame_left, 
                                                                       values=["Light", "Dark", "System"],
                                                                       command=self.change_appearance_mode)
        self.appearance_mode_optionemenu.grid(row=11, column=0, columnspan=2, padx=(10, 10), pady=(0, 30), sticky="ew")

    def initialize_time_tab_widgets(self):
        """
        Initialize widgets in the time_tab section of the notebook.
        """
        self.start_date_label = customtkinter.CTkLabel(master=self.time_tab, text="Start Date:")
        self.start_date_label.grid(row=0, column=0, padx=10, pady=(10, 0), sticky="w")
        self.start_date_calendar = DateEntry(master=self.time_tab, year=DEFAULT_START_YEAR, month=DEFAULT_START_MONTH, 
                                             day=DEFAULT_START_DAY, background='darkblue', foreground='white', borderwidth=2)
        self.start_date_calendar.grid(row=1, column=0, padx=10, pady=(0, 10), sticky="w")

        self.start_time_label = customtkinter.CTkLabel(master=self.time_tab, text="Start Time:")
        self.start_time_label.grid(row=0, column=1, padx=10, pady=(10, 0), sticky="w")
        self.start_time = TimeSelector(self.time_tab, DEFAULT_HOUR, DEFAULT_MINUTE)
        self.start_time.grid(row=1, column=1, padx=10, pady=(0, 10), sticky="e")

        self.end_date_label = customtkinter.CTkLabel(master=self.time_tab, text="End Date:")
        self.end_date_label.grid(row=2, column=0, padx=10, pady=(10, 0), sticky="w")
        self.end_date_calendar = DateEntry(master=self.time_tab, year=DEFAULT_END_YEAR, month=DEFAULT_END_MONTH, 
                                           day=DEFAULT_END_DAY, background='darkblue', foreground='white', borderwidth=2)
        self.end_date_calendar.grid(row=3, column=0, padx=10, pady=(0, 10), sticky="w")

        self.end_time_label = customtkinter.CTkLabel(master=self.time_tab, text="End Time:")
        self.end_time_label.grid(row=2, column=1, padx=10, pady=(10, 0), sticky="w")
        self.end_time = TimeSelector(self.time_tab, '23', '59')
        self.end_time.grid(row=3, column=1, padx=10, pady=(0, 10), sticky="e")

        self.timestep_label = customtkinter.CTkLabel(master=self.time_tab, text="Timestep:")
        self.timestep_label.grid(row=4, column=0, padx=10, pady=(10, 0), sticky="w")
        self.timestep = TimeStepSelector(self.time_tab, DEFAULT_TIME_STEP_HOUR)
        self.timestep.grid(row=4, column=1, padx=10, pady=(10, 0), sticky="e")

    def initialize_data_tab_widgets(self):
        """
        Initialize widgets in the data_tab section of the notebook.
        """
        preset_label = tk.Label(self.data_tab, text="Preset:", width=8)
        preset_label.grid(row=0, column=0, padx=2, sticky="w")

        wrf_check = tk.Checkbutton(self.data_tab, text="WRF", variable=self.wrf_var, command=self.on_wrf_checked)
        wrf_check.grid(row=0, column=1, sticky="ew")

        epw_check = tk.Checkbutton(self.data_tab, text="EPW", variable=self.epw_var, command=self.on_epw_checked)
        epw_check.grid(row=0, column=2, sticky="ew")
        

        source_label = tk.Label(self.data_tab, text="Source:", width=8)
        source_label.grid(row=1, column=0, padx=2, sticky="w")

        self.source_combobox = ttk.Combobox(self.data_tab, values=DATA_SOURCES, state='readonly')
        self.source_combobox.grid(row=1, column=1, columnspan=2, padx=5, sticky="ew")
        self.source_combobox.bind("<<ComboboxSelected>>", self.update_output_format)
        self.source_combobox.set(DEFAULT_DATA_SOURCE)
        self.rcp_combobox = ttk.Combobox(self.data_tab, values=RCP_SCENARIOS, width=4, state='disabled')
        self.rcp_combobox.grid(row=1, column=3, padx=2, sticky="w")

        data_selection_label = tk.Label(self.data_tab, text="Set:     ", width=8)
        data_selection_label.grid(row=2, column=0, padx=0, sticky="w")

        data_button = tk.Button(self.data_tab, text="Data", command=self.open_data_popup)
        level_button = tk.Button(self.data_tab, text="Level", command=self.open_level_popup)
        reset_button = tk.Button(self.data_tab, text="Reset", command=self.reset_data)
        data_button.grid(row=2, column=1, sticky="ew", padx=2)
        level_button.grid(row=2, column=2, sticky="ew", padx=2)
        reset_button.grid(row=2, column=3, sticky="ew", padx=2)

        output_format_label = tk.Label(self.data_tab, text="Output:", width=8)
        output_format_label.grid(row=3, column=0, sticky="w", padx=2, pady=(0, 10))

        self.output_format_combobox = ttk.Combobox(self.data_tab, values=OUTPUT_FORMATS, state='readonly')
        self.output_format_combobox.grid(row=3, column=1, padx=2, pady=(0, 10), columnspan=3, sticky="ew")
        self.output_format_combobox.set(DEFAULT_OUTPUT_FORMAT)
        self._presel_data()

    def on_closing(self, event=None):
        """
        Handle the closing event of the application. This method is invoked when the application window is closed.
        """
        # Any required cleanup or save state actions before closing the app
        # For example, saving application settings or state
        self.destroy()

    def on_tab_change(self, event):
        """Handle changes in tab selection."""
        event.widget.focus_set()

    def open_data_popup(self):
        """Open a popup to select data."""
        DataPopup(self, self.selected_data, self.source, self.output, title="Select Data")

    def open_level_popup(self):
        """Open a popup to select levels."""
        LevelPopup(self, self.selected_levels, self.source, self.output, title="Select Levels")

    def on_wrf_checked(self):
        if self.wrf_var.get() == 1:
            self.epw_var.set(0)
        self._presel_data()
        self.update_output_format()

    def on_epw_checked(self):
        if self.epw_var.get() == 1:
            self.wrf_var.set(0)
        self._presel_data()
        self.update_output_format()

    def _presel_data(self):
        if self.wrf_var.get() == 1:
            for cat in self.selected_data[self.source_combobox.get()]:
                for tab in self.selected_data[self.source_combobox.get()][cat]:
                    for item in self.selected_data[self.source_combobox.get()][cat][tab]:
                        self.selected_data[self.source_combobox.get()][cat][tab][item]["is_loaded"] = bool(self.selected_data[self.source_combobox.get()][cat][tab][item]["is_WRF"])
        elif self.epw_var.get() == 1:
            for cat in self.selected_data[self.source_combobox.get()]:
                for tab in self.selected_data[self.source_combobox.get()][cat]:
                    for item in self.selected_data[self.source_combobox.get()][cat][tab]:
                        self.selected_data[self.source_combobox.get()][cat][tab][item]["is_loaded"] = bool(self.selected_data[self.source_combobox.get()][cat][tab][item]["is_EPW"])

    def update_output_format(self, event=None):
        self.source = self.source_combobox.get()
        wrf = self.wrf_var.get()
        epw = self.epw_var.get()

        """Logic to set default value in Output Format Combobox"""
        if wrf and self.source in ["ERA5 - Reanalysis", "NCAR/GFS", "NCAR/RCP"]:
            self.output_format_combobox.set("Grib")
        elif wrf and self.source == "NCAR/ERA":
            self.output_format_combobox.set("NetCDF")
        elif epw:
            self.output_format_combobox.set("EPW")
        else:
            self.output_format_combobox.set("CSV")

        """Sets the output type based on conditions."""
        if wrf:
            self.output = "WRF"
        elif epw:
            self.output = "EPW"
        else:
            self.output = "USER"

        """Configures the RCP combobox based on the source."""
        if self.source == "NCAR/RCP":
            self.rcp_combobox.config(state='readonly')
            self.rcp_combobox.set("8.5")
        else:
            self.rcp_combobox.set("")
            self.rcp_combobox.config(state='disabled')

        """Configures the timestep based on the source."""
        if self.source in ["NCAR/GFS", "NCAR/RCP"]:
            self.timestep.set_time_step(6)
        else:
            self.timestep.set_time_step(1)

    def reset_data(self):
        self.selected_data   = json.load(open(DATA_LIST_PATH, 'r'))
        self.selected_levels = json.load(open(DEFAULT_LEVELS_PATH,'r'))
        self._presel_data()

    def validate_time(self, *args):
        time_text = self.start_time_var.get()
        if not re.match(r"^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$", time_text):
            print("Invalid time format. Please enter HH:MM.")  # Replace with your error handling

    def set_event(self):
        if self.selection_combobox.get() == "Marker":
            self.set_marker_event()
        elif self.selection_combobox.get() == "Area":
            self.set_area_event() 

    def clear_event(self):
        if self.selection_combobox.get() == "Marker":
            self.clear_marker_event()
        elif self.selection_combobox.get() == "Area":
            self.clear_area_event() 

    def search_event(self, event=None):
        self.map_widget.set_address(self.entry.get())

    def set_marker_event(self):
        current_position = self.map_widget.get_position()
           
        marker = self.map_widget.set_draggable_marker(current_position[0], current_position[1])

        self.marker_list.append(marker)
        self.marker_list = [element for element in self.marker_list if not element.deleted]

    def create_marker_at(self, lat, lon):
        new_marker = self.map_widget.set_draggable_marker(lat, lon)
        self.marker_list.append(new_marker)

    def clear_marker_event(self):
        for marker in self.marker_list:
            marker.delete()

    def clear_area_event(self):
        for area in self.area_list:
            area.delete()

    def change_appearance_mode(self, new_appearance_mode: str):
        customtkinter.set_appearance_mode(new_appearance_mode)

    def change_map(self, new_map: str):
        if new_map == "OpenStreetMap":
            self.map_widget.set_tile_server("https://a.tile.openstreetmap.org/{z}/{x}/{y}.png")
        elif new_map == "Google normal":
            self.map_widget.set_tile_server("https://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}&s=Ga", max_zoom=22)
        elif new_map == "Google satellite":
            self.map_widget.set_tile_server("https://mt0.google.com/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}&s=Ga", max_zoom=22)

    def on_closing(self, event=0):
        self.destroy()

    def start(self):
        self.mainloop()

    def on_resize(self, event):
        # Calculate the new width for the left frame and buttons
        width = self.winfo_width()  # Get the current width of the main window
        left_frame_width = max(width * 0.3, 150)  # 25% of total width, but not less than 150
        button_width = max(left_frame_width * 0.3, 75)  #45% of left frame width, but not less than 75

        # Update the width of the frame and buttons
        self.frame_left.configure(width=int(left_frame_width))
        self.set_button.configure(width=int(button_width))
        self.clear_button.configure(width=int(button_width))


        # Resize the Notebook widget and its tabs
        self.tabs.configure(width=int(left_frame_width - 20))  # Adjust the width of the Notebook
        
        # Update the layout
        self.frame_left.grid_propagate(False)  # Prevent the frame from resizing to fit its content

    def set_area_event(self):
        # Get the current bounding box of the displayed map
        xmin, ymin, xmax, ymax = self.get_current_bounding_box()
        area = self.map_widget.set_rectangle(xmin, ymin, xmax, ymax)
        self.area_list.append(area)
        self.area_list = [element for element in self.area_list if not element.deleted]



    def get_current_bounding_box(self):
        upper_left_osm = self.map_widget.upper_left_tile_pos
        lower_right_osm = self.map_widget.lower_right_tile_pos
        upper_left_decimal = osm_to_decimal(*upper_left_osm, round(self.map_widget.zoom))
        lower_right_decimal = osm_to_decimal(*lower_right_osm, round(self.map_widget.zoom))
        width_reduction = (lower_right_decimal[1] - upper_left_decimal[1]) * 0.12
        height_reduction = (upper_left_decimal[0] - lower_right_decimal[0]) * 0.10
        adjusted_bbox = (
            upper_left_decimal[0] - height_reduction,  # xmin
            upper_left_decimal[1] + width_reduction,   # ymin
            lower_right_decimal[0] + height_reduction, # xmax
            lower_right_decimal[1] - width_reduction   # ymax
        )
        return adjusted_bbox


    def _check_configuration(self):
        today = datetime.datetime.now()
        yesterday = today - datetime.timedelta(days=1)
        two_days_ago = today - datetime.timedelta(days=2)
        configuration_error = False

        start_date = convert_to_datetime(self.start_date_calendar.get())
        end_date = convert_to_datetime(self.end_date_calendar.get())
        timestep = self.timestep.hourstr.get()

        if self.source == "ERA5 - Reanalysis":
            if start_date < datetime.datetime(1940, 1, 1):
                messagebox.showwarning("Configuration Warning", 
                                       "Start date for ERA5 - Reanalysis data must be on or after 1940-01-01.")
                configuration_error = True
            if end_date > yesterday:
                messagebox.showwarning("Configuration Warning", 
                                       "End date for ERA5 - Reanalysis data must be on or before yesterday.")
                configuration_error = True

        elif self.source == "NCAR/ERA":
            if start_date < datetime.datetime(1940, 1, 1):
                messagebox.showwarning("Configuration Warning", 
                                       "Start date for NCAR/ERA data must be on or after 1940-01-01.")
                configuration_error = True
            if end_date > datetime.datetime(today.year - 1, 12, 31):
                messagebox.showwarning("Configuration Warning", 
                                       "End date for NCAR/ERA data must be on or before 31/12 of the previous year.")
                configuration_error = True

        elif self.source == "NCAR/GFS":
            if start_date < datetime.datetime(2015, 7, 8):
                messagebox.showwarning("Configuration Warning", 
                                       "Start date for NCAR/GFS data must be on or after 2015-07-08.")
                configuration_error = True
            if end_date > two_days_ago:
                messagebox.showwarning("Configuration Warning", 
                                       "End date for NCAR/GFS data must be within the last 48 hours.")
                configuration_error = True
            if timestep != "6":
                messagebox.showwarning("Configuration Warning", 
                                       "Time step for NCAR/GFS data must be 6.")
                configuration_error = True

        elif self.source == "NCAR/RCP":
            if start_date < datetime.datetime(2005, 1, 1):
                messagebox.showwarning("Configuration Warning", 
                                       "Start date for NCAR/RCP data must be on or after 01/01/2005.")
                configuration_error = True
            if end_date > datetime.datetime(2100, 12, 31):
                messagebox.showwarning("Configuration Warning", 
                                       "End date for NCAR/RCP data must be on or before 31/12/2100.")
                configuration_error = True
            if timestep != "6":
                messagebox.showwarning("Configuration Warning", 
                                       "Time step for NCAR/RCP data must be 6.")
                configuration_error = True

        if not self.marker_list and not self.area_list:
            messagebox.showwarning("Configuration Warning", 
                                   "Please select at least one geographic data (marker or area).")
            configuration_error = True

        return configuration_error

    def _find_place_name(self, gps_coordinates):
        """
        Placeholder function to find a place name from GPS coordinates.
        Replace this with a real geolocation service in a real-world application.
        """
        # Simulate a place name based on GPS coordinates
        if ((gps_coordinates['N_GPS'] == gps_coordinates['S_GPS'])
            and (gps_coordinates['E_GPS'] == gps_coordinates['W_GPS'])):
            return f"Place_{round(gps_coordinates['N_GPS'],2)}_{round(gps_coordinates['E_GPS'],2)}"
        else:
            return f"Place_{round(gps_coordinates['N_GPS'],2)}_{round(gps_coordinates['E_GPS'],2)}_{round(gps_coordinates['S_GPS'],2)}_{round(gps_coordinates['W_GPS'],2)}"         

    def export_data(self):
        """Export data based on user selections in the UI."""
        start_date = self.start_date_calendar.get()
        end_date = self.end_date_calendar.get()
        start_time = f"{self.start_time.hourstr.get()}:{self.start_time.minstr.get()}"
        end_time = f"{self.end_time.hourstr.get()}:{self.end_time.minstr.get()}"
        timestep = self.timestep.hourstr.get()

        # Get the preset based on checkbox values
        preset = self.output

        # Get selected values from comboboxes
        data_src = self.source
        output_format = self.output_format_combobox.get()

        if self._check_configuration():
            return
        
        output_directory = filedialog.askdirectory(title="Select Output Directory")
        if not output_directory:
            # User cancelled the dialog or closed it without selecting a directory
            messagebox.showwarning("Warning",
                                   "Export cancelled: No directory selected.")
            return

        places = []
        for marker in self.marker_list:
            coord={ "N_GPS": marker.position[0],
                    "E_GPS": marker.position[1],
                    "S_GPS": marker.position[0],
                    "W_GPS": marker.position[1]
            }
            places.append(coord)

        for area in self.area_list:
            coord={ "N_GPS": max(area.xmin,area.xmax),
                    "E_GPS": max(area.ymin,area.ymax),
                    "S_GPS": min(area.xmin,area.xmax),
                    "W_GPS": min(area.ymin,area.ymax),
            }
            places.append(coord)

        sl_params = []
        pl_params = []
        levels = []

        for category in self.selected_data[self.source]["single_level"]:
            for item, item_info in self.selected_data[self.source]["single_level"][category].items():
                if item_info["is_loaded"]:
                    sl_params.append(item_info["dl_name"])

        for category in self.selected_data[self.source]["pressure_level"]:
            for item, item_info in self.selected_data[self.source]["pressure_level"][category].items():
                if item_info["is_loaded"]:
                    pl_params.append(item_info["dl_name"])
        
        for l in self.selected_levels[self.source][self.output]:
            if self.selected_levels[self.source][self.output][l]['is_loaded']:
                levels.append(self.selected_levels[self.source][self.output][l]['db_level'])

        for place in places:
            place_name = self._find_place_name(place)
            filename = f"{place_name}.ini"

            config = configparser.ConfigParser()

            config['DEFAULT'] = {
                'Src': self.source,
                'Initial Date': self.start_date_calendar.get(),
                'Ending Date': self.end_date_calendar.get(),
                'Start Time': f"{self.start_time.hourstr.get()}:{self.start_time.minstr.get()}",
                'End Time': f"{self.end_time.hourstr.get()}:{self.end_time.minstr.get()}",
                'Time Interval': self.timestep.hourstr.get(),
                'N_GPS': place['N_GPS'],
                'E_GPS': place['E_GPS'],
                'S_GPS': place['S_GPS'],
                'W_GPS': place['W_GPS'],
                'Output': self.output_format_combobox.get(),
                'Single Level Parameters': ', '.join(sl_params),
                'Pressure Level Parameters': ', '.join(pl_params),
                'Levels': ', '.join(map(str, levels))
            }
            if self.rcp_combobox.get() != "":
                config['DEFAULT']['RCP'] = self.rcp_combobox

            sample_file_path = os.path.join(output_directory, filename)
            try:
                with open(sample_file_path, 'w') as configfile:
                    config.write(configfile)
                messagebox.showinfo("Success", f"File '{filename}' successfully saved to {output_directory}")
            except Exception as e:
                messagebox.showerror("Error", f"An error occurred while saving '{filename}': {e}")
