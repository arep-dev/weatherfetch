import tkinter
import sys
from typing import TYPE_CHECKING, Callable
from .canvas_polygon import CanvasPolygon

class CanvasRectangle(CanvasPolygon):
    def __init__(self, map_widget: "TkinterMapView", xmin, ymin, xmax, ymax, **kwargs):
        # Convert the given xmin, ymin, xmax, ymax into a position list
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        position_list = [(xmin, ymin), (xmax, ymin), (xmax, ymax), (xmin, ymax)]

        # Call the superclass constructor with the new position list
        super().__init__(map_widget, position_list, **kwargs)

        # Instance variables for dragging
        self.drag_start_x = None
        self.drag_start_y = None
        self.orig_x = 0
        self.orig_y = 0
        self.is_dragging = False
        self.is_resizing = False
        self.dx = 0
        self.dy = 0
        self.current_cursor = None

    def update_rectangle(self, xmin, ymin, xmax, ymax):
        """Update the rectangle with new coordinates."""
        self.position_list = [(xmin, ymin), (xmax, ymin), (xmax, ymax), (xmin, ymax)]
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax

    def draw(self, move=False):
        # check if number of positions in position_list has changed
        new_line_length = self.last_position_list_length != len(self.position_list)
        self.last_position_list_length = len(self.position_list)

        # get current tile size of map widget
        widget_tile_width = self.map_widget.lower_right_tile_pos[0] - self.map_widget.upper_left_tile_pos[0]
        widget_tile_height = self.map_widget.lower_right_tile_pos[1] - self.map_widget.upper_left_tile_pos[1]

        # if only moving happened and len(self.position_list) did not change, shift current positions, else calculate new position_list
        if move is True and self.last_upper_left_tile_pos is not None and new_line_length is False:
            x_move = ((self.last_upper_left_tile_pos[0] - self.map_widget.upper_left_tile_pos[0]) / widget_tile_width) * self.map_widget.width
            y_move = ((self.last_upper_left_tile_pos[1] - self.map_widget.upper_left_tile_pos[1]) / widget_tile_height) * self.map_widget.height

            for i in range(0, len(self.position_list) * 2, 2):
                self.canvas_polygon_positions[i] += x_move
                self.canvas_polygon_positions[i + 1] += y_move
        else:
            self.canvas_polygon_positions = []
            for position in self.position_list:
                canvas_position = self.get_canvas_pos(position, widget_tile_width, widget_tile_height)
                self.canvas_polygon_positions.append(canvas_position[0])
                self.canvas_polygon_positions.append(canvas_position[1])

        if not self.deleted:
            if self.canvas_polygon is None:
                self.map_widget.canvas.delete(self.canvas_polygon)
                self.canvas_polygon = self.map_widget.canvas.create_polygon(self.canvas_polygon_positions,
                                                                            width=self.border_width,
                                                                            outline=self.outline_color,
                                                                            joinstyle=tkinter.ROUND,
                                                                            stipple="gray25",
                                                                            tag="polygon")
                if self.fill_color is None:
                    self.map_widget.canvas.itemconfig(self.canvas_polygon, fill="")
                else:
                    self.map_widget.canvas.itemconfig(self.canvas_polygon, fill=self.fill_color)

                self.map_widget.canvas.tag_bind(self.canvas_polygon, "<Button-1>", self.clic)
                self.map_widget.canvas.tag_bind(self.canvas_polygon,"<B1-Motion>", self.mouse_move)
                self.map_widget.canvas.tag_bind(self.canvas_polygon,"<ButtonRelease-1>", self.release)
                self.map_widget.canvas.tag_bind(self.canvas_polygon, "<Motion>", self.update_cursor)
                self.map_widget.canvas.tag_bind(self.canvas_polygon, "<Leave>", self.mouse_leave)
                self.map_widget.canvas.tag_bind(self.canvas_polygon, "<Button-3>", self.mouse_right_click)
            else:
                self.map_widget.canvas.coords(self.canvas_polygon, self.canvas_polygon_positions)
        else:
            self.map_widget.canvas.delete(self.canvas_polygon)
            self.canvas_polygon = None

        self.map_widget.manage_z_order()
        self.last_upper_left_tile_pos = self.map_widget.upper_left_tile_pos

    # Rest of your methods...

    def clic(self, event):
        """ Start the dragging process. """
        if self.current_cursor == "hand2":
            self.start_drag(event)
        elif self.current_cursor not in ["arrow"]:
            self.start_release(event)

    def mouse_move(self, event):
        """ Handle the dragging of the rectangle. """
        if self.is_dragging:
            self.drag(event)
        elif self.is_resizing:
            self.resize(event)

    def release(self, event):
        if self.is_dragging:
            self.end_drag(event)
        elif self.is_resizing:
            self.end_resize(event)

    def update_cursor(self, event):
        # Threshold distance to detect if cursor is near the edge or corner
        edge_threshold = 10  # pixels

        # Get the current position of the cursor relative to the canvas
        cursor_x, cursor_y = event.x, event.y

        # Get the bounding box of the rectangle
        bbox = self.map_widget.canvas.bbox(self.canvas_polygon)

        if bbox is not None:
            x1, y1, x2, y2 = bbox

            # Check corners
            if abs(cursor_x - x1) <= edge_threshold and abs(cursor_y - y1) <= edge_threshold:
                cursor = "top_left_corner"
            elif abs(cursor_x - x2) <= edge_threshold and abs(cursor_y - y1) <= edge_threshold:
                cursor = "top_right_corner"
            elif abs(cursor_x - x1) <= edge_threshold and abs(cursor_y - y2) <= edge_threshold:
                cursor = "bottom_left_corner"
            elif abs(cursor_x - x2) <= edge_threshold and abs(cursor_y - y2) <= edge_threshold:
                cursor = "bottom_right_corner"
            # Check edges
            elif abs(cursor_x - x1) <= edge_threshold:
                cursor = "left_side"
            elif abs(cursor_x - x2) <= edge_threshold:
                cursor = "right_side"
            elif abs(cursor_y - y1) <= edge_threshold:
                cursor = "top_side"
            elif abs(cursor_y - y2) <= edge_threshold:
                cursor = "bottom_side"
            elif x1 < cursor_x < x2 and y1 < cursor_y < y2:
                cursor = "hand2"
            else:
                cursor = "arrow"
            self.current_cursor = cursor
            self.map_widget.canvas.config(cursor=cursor)

    def start_drag(self, event):
        self.drag_start_x = event.x
        self.drag_start_y = event.y
        if (self.dx == 0) and (self.dy == 0):
            self.orig_x = self.drag_start_x
            self.orig_y = self.drag_start_y
        self.is_dragging = True
        self.is_resizing = False

    def start_release(self, event):
        self.resize_start_x = event.x
        self.resize_start_y = event.y
        self.is_resizing = True
        self.is_dragging = False
        self.resize_side = self.current_cursor  # Storing which side or corner is being resized

    def drag(self, event):
        dx = event.x - self.drag_start_x
        dy = event.y - self.drag_start_y
        self.dx += dx
        self.dy += dy
        # Update the start position for the next motion
        self.drag_start_x = event.x
        self.drag_start_y = event.y
        # Move the rectangle by the amount of change in x and y
        self.map_widget.canvas.move(self.canvas_polygon, dx, dy)

    def resize(self, event):
        """ Handle the resizing of the rectangle. """
        if self.is_resizing:
            dx = event.x - self.resize_start_x
            dy = event.y - self.resize_start_y
            c = 0.03*1/(2.0 ** (self.map_widget.zoom+1)) # velocity
            # Update the position list based on the resize side/corner
            if self.resize_side == "top_left_corner":
                # Example for top left corner resize
                self.xmin += -c*dy
                self.ymin += c*dx
                if self.xmin < self.xmax:
                    self.xmin = self.xmax
                if self.ymin > self.ymax:
                    self.ymin = self.ymax
            elif self.resize_side == "top_right_corner":
                # Example for top left corner resize
                self.xmin += -c*dy
                self.ymax += c*dx
                if self.xmin < self.xmax:
                    self.xmin = self.xmax
                if self.ymin > self.ymax:
                    self.ymax = self.ymin
            elif self.resize_side == "bottom_left_corner":
                # Example for top left corner resize
                self.xmax += -c*dy
                self.ymin += c*dx
                if self.xmin < self.xmax:
                    self.xmax = self.xmin
                if self.ymin > self.ymax:
                    self.ymin = self.ymax
            elif self.resize_side == "bottom_right_corner":
                # Example for top left corner resize
                self.xmax += -c*dy
                self.ymax += c*dx
                if self.xmin < self.xmax:
                    self.xmax = self.xmin
                if self.ymin > self.ymax:
                    self.ymax = self.ymax
            elif self.resize_side == "left_side":
                # Example for right side resize
                self.ymin += c*dx
                if self.ymin > self.ymax:
                    self.ymin = self.ymax
            elif self.resize_side == "right_side":
                # Example for right side resize
                self.ymax += c*dx
                if self.ymin > self.ymax:
                    self.ymax = self.ymin
            elif self.resize_side == "top_side":
                # Example for right side resize
                self.xmin += -c*dy
                if self.xmin < self.xmax:
                    self.xmin = self.xmax
            elif self.resize_side == "bottom_side":
                # Example for right side resize
                self.xmax += -c*dy
                if self.xmin < self.xmax:
                    self.xmax = self.xmin

            # Add similar conditions for other sides/corners

            self.update_rectangle(self.xmin, self.ymin, self.xmax, self.ymax)
            self.draw()

    def end_resize(self, event):
        """ End the resizing process. """
        self.is_resizing = False

    def end_drag(self,event):
        pos_x = self.orig_x+self.dx
        pos_y = self.orig_y+self.dy
        pos_x, pos_y = self.map_widget.convert_canvas_coords_to_decimal_coords(pos_x, pos_y)
        orig_x, orig_y = self.map_widget.convert_canvas_coords_to_decimal_coords(self.orig_x, self.orig_y)
        dx = pos_x - orig_x
        dy = pos_y - orig_y
        self.update_rectangle(self.xmin+dx, self.ymin+dy, self.xmax+dx, self.ymax+dy)
        self.is_dragging = False
        self.dx = 0
        self.dy = 0
        self.orig_x = 0
        self.orig_y = 0
        self.draw()

    def mouse_right_click(self, event):
        # Create a popup menu
        m = tkinter.Menu(self.map_widget, tearoff=0)
        # Define a nested function for deletion with confirmation
        def confirm_deletion():
            response = tkinter.messagebox.askyesno("Confirm Deletion", "Do you want to delete this item?")
            if response:
                self.delete()  # Call the delete method if 'Yes' is clicked

        # Add the deletion option to the menu
        m.add_command(label="Delete", command=confirm_deletion)

        # Display the menu
        m.tk_popup(event.x_root, event.y_root)

    def delete(self):
        self.deleted = True
        self.draw()
