import tkinter
import sys
from typing import TYPE_CHECKING, Callable
from .canvas_position_marker import CanvasPositionMarker

class CanvasDraggableMarker(CanvasPositionMarker):
    def __init__(self, 
                map_widget: "TkinterMapView", 
                position: tuple,
                **kwargs
                ):
        self.map_widget = map_widget
        self.position = position

        # Call the superclass constructor with the new position list
        super().__init__(map_widget, position, **kwargs)

        # Instance variables for dragging
        self.drag_start_x = None
        self.drag_start_y = None
        self.orig_x = 0
        self.orig_y = 0
        self.is_dragging = False
        self.dx = 0
        self.dy = 0
        self.current_cursor = None

    def draw(self, event=None):
        canvas_pos_x, canvas_pos_y = self.get_canvas_pos(self.position)

        if not self.deleted:
            if 0 - 50 < canvas_pos_x < self.map_widget.width + 50 and 0 < canvas_pos_y < self.map_widget.height + 70:

                # draw icon image for marker
                if self.icon is not None:
                    if self.canvas_icon is None:
                        self.canvas_icon = self.map_widget.canvas.create_image(canvas_pos_x, canvas_pos_y,
                                                                               anchor=self.icon_anchor,
                                                                               image=self.icon,
                                                                               tag="marker")
                        self.map_widget.canvas.tag_bind(self.canvas_icon, "<Enter>", self.mouse_enter)
                        self.map_widget.canvas.tag_bind(self.canvas_icon, "<Button-1>", self.clic)
                        self.map_widget.canvas.tag_bind(self.canvas_icon,"<B1-Motion>", self.mouse_move)
                        self.map_widget.canvas.tag_bind(self.canvas_icon,"<ButtonRelease-1>", self.release)
                        self.map_widget.canvas.tag_bind(self.canvas_icon, "<Leave>", self.mouse_leave)   
                        self.map_widget.canvas.tag_bind(self.canvas_icon, "<Button-3>", self.mouse_right_click)                                                 
                    else:
                        self.map_widget.canvas.coords(self.canvas_icon, canvas_pos_x, canvas_pos_y)

                # draw standard icon shape
                else:
                    if self.polygon is None:
                        self.polygon = self.map_widget.canvas.create_polygon(canvas_pos_x - 14, canvas_pos_y - 23,
                                                                             canvas_pos_x, canvas_pos_y,
                                                                             canvas_pos_x + 14, canvas_pos_y - 23,
                                                                             fill=self.marker_color_outside, width=2,
                                                                             outline=self.marker_color_outside, tag="marker")
                        self.map_widget.canvas.tag_bind(self.polygon, "<Enter>", self.mouse_enter)
                        self.map_widget.canvas.tag_bind(self.polygon, "<Button-1>", self.clic)
                        self.map_widget.canvas.tag_bind(self.polygon,"<B1-Motion>", self.mouse_move)
                        self.map_widget.canvas.tag_bind(self.polygon,"<ButtonRelease-1>", self.release)
                        self.map_widget.canvas.tag_bind(self.polygon, "<Leave>", self.mouse_leave)
                        self.map_widget.canvas.tag_bind(self.polygon, "<Button-3>", self.mouse_right_click)
                    else:
                        self.map_widget.canvas.coords(self.polygon,
                                                      canvas_pos_x - 14, canvas_pos_y - 23,
                                                      canvas_pos_x, canvas_pos_y,
                                                      canvas_pos_x + 14, canvas_pos_y - 23)
                    if self.big_circle is None:
                        self.big_circle = self.map_widget.canvas.create_oval(canvas_pos_x - 14, canvas_pos_y - 45,
                                                                             canvas_pos_x + 14, canvas_pos_y - 17,
                                                                             fill=self.marker_color_circle, width=6,
                                                                             outline=self.marker_color_outside, tag="marker")
                        self.map_widget.canvas.tag_bind(self.big_circle, "<Enter>", self.mouse_enter)
                        self.map_widget.canvas.tag_bind(self.big_circle, "<Button-1>", self.clic)
                        self.map_widget.canvas.tag_bind(self.big_circle,"<B1-Motion>", self.mouse_move)
                        self.map_widget.canvas.tag_bind(self.big_circle,"<ButtonRelease-1>", self.release)
                        self.map_widget.canvas.tag_bind(self.big_circle, "<Leave>", self.mouse_leave)
                        self.map_widget.canvas.tag_bind(self.big_circle, "<Button-3>", self.mouse_right_click)
                    else:
                        self.map_widget.canvas.coords(self.big_circle,
                                                      canvas_pos_x - 14, canvas_pos_y - 45,
                                                      canvas_pos_x + 14, canvas_pos_y - 17)

                if self.text is not None:
                    if self.canvas_text is None:
                        self.canvas_text = self.map_widget.canvas.create_text(canvas_pos_x, canvas_pos_y + self.text_y_offset,
                                                                              anchor=tkinter.S,
                                                                              text=self.text,
                                                                              fill=self.text_color,
                                                                              font=self.font,
                                                                              tag=("marker", "marker_text"))
                        self.map_widget.canvas.tag_bind(self.canvas_text, "<Enter>", self.mouse_enter)
                        self.map_widget.canvas.tag_bind(self.canvas_text, "<Button-1>", self.clic)
                        self.map_widget.canvas.tag_bind(self.canvas_text,"<B1-Motion>", self.mouse_move)
                        self.map_widget.canvas.tag_bind(self.canvas_text,"<ButtonRelease-1>", self.release)
                        self.map_widget.canvas.tag_bind(self.canvas_text, "<Leave>", self.mouse_leave)
                        self.map_widget.canvas.tag_bind(self.canvas_text, "<Button-3>", self.mouse_right_click)
                    else:
                        self.map_widget.canvas.coords(self.canvas_text, canvas_pos_x, canvas_pos_y + self.text_y_offset)
                        self.map_widget.canvas.itemconfig(self.canvas_text, text=self.text)
                else:
                    if self.canvas_text is not None:
                        self.map_widget.canvas.delete(self.canvas_text)

                if self.image is not None and self.image_zoom_visibility[0] <= self.map_widget.zoom <= self.image_zoom_visibility[1]\
                        and not self.image_hidden:

                    if self.canvas_image is None:
                        self.canvas_image = self.map_widget.canvas.create_image(canvas_pos_x, canvas_pos_y + (self.text_y_offset - 30),
                                                                                anchor=tkinter.S,
                                                                                image=self.image,
                                                                                tag=("marker", "marker_image"))
                    else:
                        self.map_widget.canvas.coords(self.canvas_image, canvas_pos_x, canvas_pos_y + (self.text_y_offset - 30))
                else:
                    if self.canvas_image is not None:
                        self.map_widget.canvas.delete(self.canvas_image)
                        self.canvas_image = None
            else:
                self.map_widget.canvas.delete(self.canvas_icon)
                self.map_widget.canvas.delete(self.canvas_text)
                self.map_widget.canvas.delete(self.polygon)
                self.map_widget.canvas.delete(self.big_circle)
                self.map_widget.canvas.delete(self.canvas_image)
                self.canvas_text, self.polygon, self.big_circle, self.canvas_image, self.canvas_icon = None, None, None, None, None
            self.map_widget.manage_z_order()

    def clic(self, event):
        self.drag_start_x = event.x
        self.drag_start_y = event.y
        if (self.dx == 0) and (self.dy == 0):
            self.orig_x = self.drag_start_x
            self.orig_y = self.drag_start_y
        self.is_dragging = True

    def mouse_move(self, event):
        dx = event.x - self.drag_start_x
        dy = event.y - self.drag_start_y
        self.dx += dx
        self.dy += dy
        # Update the start position for the next motion
        self.drag_start_x = event.x
        self.drag_start_y = event.y
        # Move the rectangle by the amount of change in x and y
        self.map_widget.canvas.move(self.polygon, dx, dy)
        self.map_widget.canvas.move(self.big_circle, dx, dy)

    def release(self,event):
        pos_x = self.orig_x+self.dx
        pos_y = self.orig_y+self.dy
        pos_x, pos_y = self.map_widget.convert_canvas_coords_to_decimal_coords(pos_x, pos_y)
        orig_x, orig_y = self.map_widget.convert_canvas_coords_to_decimal_coords(self.orig_x, self.orig_y)
        dx = pos_x - orig_x
        dy = pos_y - orig_y
        x0, y0 = self.position
        self.position = (x0 + dx, y0 + dy)
        self.is_dragging = False
        self.dx = 0
        self.dy = 0
        self.orig_x = 0
        self.orig_y = 0
        self.draw()

    def mouse_right_click(self, event):
        # Create a popup menu
        m = tkinter.Menu(self.map_widget, tearoff=0)
        # Define a nested function for deletion with confirmation
        def confirm_deletion():
            response = tkinter.messagebox.askyesno("Confirm Deletion", "Do you want to delete this item?")
            if response:
                self.delete()  # Call the delete method if 'Yes' is clicked

        # Add the deletion option to the menu
        m.add_command(label="Delete", command=confirm_deletion)

        # Display the menu
        m.tk_popup(event.x_root, event.y_root)

    def delete(self):
        self.map_widget.canvas.delete(self.polygon)
        self.map_widget.canvas.delete(self.big_circle)
        self.map_widget.canvas.delete(self.canvas_text)
        self.map_widget.canvas.delete(self.canvas_icon)
        self.map_widget.canvas.delete(self.canvas_image)

        self.polygon, self.big_circle, self.canvas_text, self.canvas_image, self.canvas_icon = None, None, None, None, None
        self.deleted = True
        self.draw()