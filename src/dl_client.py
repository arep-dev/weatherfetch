import os
import tkinter as tk
from tkinter import ttk, filedialog, messagebox
import customtkinter
import configparser
import threading
import ctypes
from datetime import datetime, timedelta
import get_emcwf_era as emcwf

def read_config(file_path):
    """
    Reads the configuration file and returns the settings as a dictionary.
    """
    config = configparser.ConfigParser()
    config.read(file_path)
    return config['DEFAULT']

def add_months(dt, months):
    # Function to add a specific number of months to a date
    month = dt.month - 1 + months
    year = dt.year + month // 12
    month = month % 12 + 1
    day = min(dt.day, [31, (29 if year % 4 == 0 and not year % 100 == 0 or year % 400 == 0 else 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month - 1])
    return datetime(year, month, day)

def calculate_interval(start_date, end_date, n_gps, s_gps):
    start_year = int(start_date.split('-')[0])
    end_year = int(end_date.split('-')[0])
    years = end_year - start_year + 1

    area_height = n_gps - s_gps  # Rough measure of area height in degrees

    # Adjust interval based on years and area size
    if area_height > 5 or years > 5:
        return 365  # Yearly
    elif years > 2:
        return 182  # Every 6 months
    else:
        return 91  # Every 3 months

def generate_date_ranges(start_date, end_date, interval):
    start = datetime.strptime(start_date, '%d/%m/%Y')
    end = datetime.strptime(end_date, '%d/%m/%Y')
    ranges = []

    if interval == "no split":
        ranges.append((start_date, end_date))
    elif interval == "yearly":
        while start < end:
            next_end = min(add_months(start, 12) - timedelta(days=1), end)
            ranges.append((start.strftime('%d/%m/%Y'), next_end.strftime('%d/%m/%Y')))
            start = next_end + timedelta(days=1)
    elif interval == "by semester":
        while start < end:
            next_end = min(add_months(start, 6) - timedelta(days=1), end)
            ranges.append((start.strftime('%d/%m/%Y'), next_end.strftime('%d/%m/%Y')))
            start = next_end + timedelta(days=1)
    elif interval == "by trimester":
        while start < end:
            next_end = min(add_months(start, 3) - timedelta(days=1), end)
            ranges.append((start.strftime('%d/%m/%Y'), next_end.strftime('%d/%m/%Y')))
            start = next_end + timedelta(days=1)
    elif interval == "monthly":
        while start < end:
            next_end = min(add_months(start, 1) - timedelta(days=1), end)
            ranges.append((start.strftime('%d/%m/%Y'), next_end.strftime('%d/%m/%Y')))
            start = next_end + timedelta(days=1)
    elif interval == "daily":
        while start < end:
            next_end = min(start + timedelta(days=1), end)
            ranges.append((start.strftime('%d/%m/%Y'), next_end.strftime('%d/%m/%Y')))
            start = next_end

    return ranges
def select_config_file():
    filename = filedialog.askopenfilename(filetypes=[("Config Files", "*.ini")])
    if filename:
        config_file_path.set(filename)
        config = read_config(filename)
        interval.set(calculate_interval(config['Initial Date'], config['Ending Date'], float(config['N_GPS']), float(config['S_GPS'])))


class ToolTip(object):
    """
    Create a tooltip for a given widget.
    """
    def __init__(self, widget, text='widget info'):
        self.waittime = 500     # milliseconds
        self.wraplength = 180   # pixels
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.id = None
        self.tw = None

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hide()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.show)

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def show(self):
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # Creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                       background="#ffffff", relief='solid', borderwidth=1,
                       wraplength = self.wraplength)
        label.pack(ipadx=1)

    def hide(self):
        tw = self.tw
        self.tw= None
        if tw:
            tw.destroy()

class Downloader(customtkinter.CTk):
    def __init__(self, *args, **kwargs):
        """
        Initialize the main application window and its widgets.
        """
        super().__init__(*args, **kwargs)
        self.title("Data downloader")
        self.geometry(f"{275}x{160}")
        self.minsize(275, 275)
        self.maxsize(350, 275)
        self.config_file_path = tk.StringVar(self)
        self.output_path = tk.StringVar(self)
        self.dt_split = "no split"
        self.download_thread = None  # Initialize the download thread as None
        self.output_manually_set = False

        self.initialize_widget()


    def start(self):
        self.mainloop()

    def initialize_widget(self):
        self.frame = customtkinter.CTkFrame(master=self, corner_radius=0)
        self.frame.grid(row=0, column=0, padx=0, pady=0, sticky="nsew")
        self.frame.grid_rowconfigure(0, weight=0, pad=0)
        self.frame.grid_rowconfigure(1, weight=0, pad=0)
        self.frame.grid_rowconfigure(2, weight=0, pad=0)
        self.frame.grid_rowconfigure(3, weight=0, pad=0)
        self.frame.grid_rowconfigure(4, weight=0, pad=0)
        self.frame.grid_rowconfigure(5, weight=1, pad=0)
        self.frame.grid_columnconfigure(0, weight=1, pad=0)
        self.sel_label = customtkinter.CTkLabel(self.frame, text="Select Configuration File:", anchor="w")
        self.sel_label.grid(row=0, column=0, columnspan=1, padx=(5, 2), pady=(10, 0), sticky="w")
        self.sel_button = tk.Button(self.frame, text="...", command=self._select_config_file, width=3)
        self.sel_button.grid(row=0, column=1, columnspan=1, padx=(2, 5), pady=(5,0), sticky="e")
        self.sel_file_path = tk.Entry(self.frame, textvariable=self.config_file_path, state='readonly')
        self.sel_file_path.grid(row=1, column=0, columnspan=2, padx=(5, 5), pady=(2, 10), sticky="ew")
        self.out_label = customtkinter.CTkLabel(self.frame, text="Set Output", anchor="w")
        self.out_label.grid(row=2, column=0, columnspan=1, padx=(5, 2), pady=(10, 0), sticky="w")
        self.out_button = tk.Button(self.frame, text="...", command=self._select_output_path, width=3)
        self.out_button.grid(row=2, column=0, columnspan=2, padx=(2, 5), pady=(5,0), sticky="e")
        self.sel_output_path = tk.Entry(self.frame, textvariable=self.output_path, state='readonly')
        self.sel_output_path.grid(row=3, column=0, columnspan=2, padx=(5, 5), pady=(2, 10), sticky="ew")
        self.spt_label = customtkinter.CTkLabel(self.frame, text="Split Time for download:", anchor="w")
        self.spt_label.grid(row=4, column=0, columnspan=1, padx=(5, 5), pady=(0, 5), sticky="w")
        self.spt_combobox = ttk.Combobox(self.frame, values=["no split", "yearly", "by semester", "by trimester", "monthly", "daily"], state='readonly', width=12)
        self.spt_combobox.grid(row=4, column=1, columnspan=1, padx=5, pady=(0, 5), sticky="ew")
        self.spt_combobox.set("no split")
        separator = ttk.Separator(self.frame, orient='horizontal')
        separator.grid(row=5, column=0, columnspan=2, padx=(5, 5), pady=(0, 5), sticky="ew")
        self.dl_button = tk.Button(self.frame, text="Download", command=self._run)
        self.dl_button.grid(row=6, column=0, columnspan=1, padx=(5, 5), pady=(5, 10), sticky="ew")
        self.abt_button = tk.Button(self.frame, text="Abort", command=self._abort)
        self.abt_button.grid(row=6, column=1, columnspan=1, padx=(5, 5), pady=(5, 10), sticky="ew")
        # Attach the tooltip to self.spt_label
        tooltip_text = ("Selecting 'No split' might result in a large file size, " +
                        "potentially leading to download issues due to server limitations.")
        ToolTip(self.spt_label, text=tooltip_text)
        # Bind resize event
        self.bind("<Configure>", self._on_resize)
        self.spt_combobox.bind("<<ComboboxSelected>>", self._on_value_change)

    def _on_resize(self, event):
        # Calculate the new width for the left frame and buttons
        width = self.winfo_width()  # Get the current width of the main window
        frame_width = width  # 25% of total width, but not less than 150
        # Update the width of the frame and buttons
        self.frame.configure(width=int(frame_width), height = int(self.winfo_width()) )
        self.frame.grid_propagate(False)  # Prevent the frame from resizing to fit its content

    def _select_config_file(self):
        filename = filedialog.askopenfilename(filetypes=[("Select a Config File", "*.ini")])
        self.focus_set()
        if filename:
            self.config_file_path.set(filename)
        if self.output_manually_set == False:
            self.output_path.set(os.path.dirname(filename))

    def _select_output_path(self):    
        foldername = filedialog.askdirectory(mustexist=False, title="Select a Folder")
        self.focus_set()
        if foldername:
            self.output_path.set(foldername)
            self.output_manually_set == True

    def _on_value_change(self,event):
        self.focus_set()
        event.widget.selection_clear()
        self.dt_split = self.spt_combobox.get()

    def _run(self):
        if not self.config_file_path.get():
            messagebox.showwarning("Missing file Warning", 
                                   "Please select configuration file for download.")
        else:
            if self.download_thread and self.download_thread.is_alive():
                messagebox.showwarning("Download in Progress", 
                                       "A download is already in progress. Please wait until it completes.")
            else:
                os.chdir(self.output_path.get())
                self.cfg = read_config(self.config_file_path.get())
                dt_range = generate_date_ranges(self.cfg["initial date"], self.cfg["ending date"], self.dt_split)
                if self.cfg["src"]=="ERA5 - Reanalysis":
                    self.download_thread = threading.Thread(target=self._dl, args=(self.cfg, dt_range), daemon=True)
                    self.download_thread.start()

    def _dl(self, cfg, dt_range):
        if cfg["src"]=="ERA5 - Reanalysis":
            emcwf.dl(cfg, dt_range)


    def _abort(self):
        if self.download_thread and self.download_thread.is_alive():
            # Confirmation dialog
            response = messagebox.askyesno("Confirm Abort", 
                                        "Are you sure you want to abort the ongoing download? " 
                                        "This action cannot be undone and may leave partial or corrupted data.")

            if response:  # If the user confirms the action
                self._stop_thread(self.download_thread)
                messagebox.showinfo("Aborted", "The download has been successfully aborted.")
                print("Download process forcefully terminated.\n")
        else:
            messagebox.showinfo("Abort Operation", "There is no active download to abort.")

    def _stop_thread(self, thread):
        """Forcefully stops a thread."""
        if not thread.is_alive():
            return

        thread_id = None
        for id, t in threading._active.items():
            if t is thread:
                thread_id = id
                break

        if thread_id:
            res = ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id), ctypes.py_object(SystemExit))
            if res > 1:
                ctypes.pythonapi.PyThreadState_SetAsyncExc(ctypes.c_long(thread_id), None)
                messagebox.showerror("SIGTERM", "Exception raise failure.")
        else:
            messagebox.showerror("Thread not found.")
