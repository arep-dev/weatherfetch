import json
import os

# General Application Constants
APP_NAME = "WeatherFetch"
APP_WIDTH = 900
APP_HEIGHT = 560

# Default Settings for Map
DEFAULT_MAP = "OpenStreetMap"
DEFAULT_TILE_SERVER = "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png"
MAX_ZOOM_LEVEL = 22
DEFAULT_ZOOM_LEVEL = 4
DEFAULT_ADRESS = "Europe"

# Default Time and Date Settings
DEFAULT_START_YEAR = 2001
DEFAULT_START_MONTH = 1
DEFAULT_START_DAY = 1
DEFAULT_END_YEAR = 2011
DEFAULT_END_MONTH = 12
DEFAULT_END_DAY = 31
DEFAULT_HOUR = '00'
DEFAULT_MINUTE = '00'
DEFAULT_TIME_STEP_HOUR = '01'

# Default Appearance Mode
DEFAULT_APPEARANCE_MODE = "Light"

# Data Sources
DATA_SOURCES = ["ERA5 - Reanalysis"]
RCP_SCENARIOS = ["4.5", "6.0" , "8.5"]
DEFAULT_DATA_SOURCE = "ERA5 - Reanalysis"
DEFAULT_OUTPUT = "WRF"
DATA_LIST_PATH = os.path.join(os.path.dirname(__file__), '..', 'resources', 'json', 'LIST_PARAMS.json')
DEFAULT_LEVELS_PATH =  os.path.join(os.path.dirname(__file__), '..', 'resources', 'json', 'LIST_LEVELS.json')

# Output Formats
OUTPUT_FORMATS = ["CSV", "EPW", "Grib", "NetCDF"]
DEFAULT_OUTPUT_FORMAT = "Grib"

# Preset Settings
PRESET_WRF = "WRF"
PRESET_EPW = "EPW"
