import cdsapi
import os
from datetime import datetime, timedelta
import xarray as xr
import pandas as pd
import netCDF4 as nc
from csv2epw import EpwConvertor

def convert_date_format(date_str):
    """
    Convert a date string from 'dd/mm/yyyy' to 'yyyy-mm-dd'.
    
    :param date_str: String representing the date in 'dd/mm/yyyy' format.
    :return: String representing the date in 'yyyy-mm-dd' format.
    """
    # Parse the date string into a datetime object
    date_obj = datetime.strptime(date_str, '%d/%m/%Y')

    # Format the datetime object into the desired string format
    return date_obj.strftime('%Y-%m-%d')

def st_format(format):
    """
    data structure output format
    """
    if format.lower() == "grib":
         return format.lower()
    else:
         return "netcdf"

def format_cdsapi_request(cfg, date_range, data_type):
    """
    Format the request for the CDS API and extend the end date by one day if the output format is 'epw'.

    :param cfg: Configuration dictionary containing request parameters.
    :param date_range: Tuple of start and end date strings in 'dd/mm/yyyy' format.
    :param data_type: Type of the data being requested.
    :return: Dictionary formatted as a request for the CDS API.
    """
    # Formatting the 'area' and 'time' parameters
    area = f"{cfg['n_gps']}/{cfg['w_gps']}/{cfg['s_gps']}/{cfg['e_gps']}"
    time = f"{cfg['start time'].split(':')[0].zfill(2)}/to/{cfg['end time'].split(':')[0].zfill(2)}/by/{cfg['time interval']}"

    # Extend end date by one day if output format is 'epw'
    end_date = date_range[1]
    if cfg['output'].lower() == "epw":
        end_date_obj = datetime.strptime(end_date, '%d/%m/%Y') + timedelta(days=1)
        end_date = end_date_obj.strftime('%d/%m/%Y')

    # Creating the request dictionary
    request = {
        'product_type': 'reanalysis',
        'format': st_format(cfg['output']),
        'date': f"{convert_date_format(date_range[0])}/{convert_date_format(end_date)}",
        'area': area,
        'time': time
    }

    if data_type == 'pl':
        request.update({
            'pressure_level': cfg['levels'].split(', '),
            'variable': cfg['pressure level parameters'].split(', ')
        })
        return 'reanalysis-era5-pressure-levels', request

    elif data_type == 'sl':
        request.update({
            'variable': cfg['single level parameters'].split(', ')
        })
        return 'reanalysis-era5-single-levels', request
    
def generate_filename(cfg, date_range, data_type):
        if ((cfg['N_GPS'] == cfg['S_GPS'])
            and (cfg['E_GPS'] == cfg['W_GPS'])):
            place_name = f"Place_{round(float(cfg['N_GPS']),2)}_{round(float(cfg['E_GPS']),2)}"
        else:
            place_name = f"Place_{round(float(cfg['N_GPS']),2)}_{round(float(cfg['E_GPS']),2)}_{round(float(cfg['S_GPS']),2)}_{round(float(cfg['W_GPS']),2)}"         
        
        start_date = convert_date_format(date_range[0]).replace('-', '')
        end_date = convert_date_format(date_range[1]).replace('-', '')
        file_type = 'pl' if data_type == 'pl' else 'sl'
        file_format = cfg['output']
        return f"{place_name}-{start_date}-{end_date}-{file_type}"

def download_from_api(cfg, date_range, data_type):
    cds_api_dataset, request = format_cdsapi_request(cfg, date_range, data_type)
    filename = generate_filename(cfg, date_range, data_type)

    if not os.path.exists(f"{filename}.{cfg['output'].lower()}"):
        c = cdsapi.Client()
        if cfg['output'].lower()=="grib":
             ext='grib'
        else:
             ext='nc'
        try:
            c.retrieve(cds_api_dataset, request,f"{filename}.{ext}")
            if cfg['output'].lower()!="grib":
                ds = xr.open_dataset(f"{filename}.{ext}")
                newds=ds.sel(longitude=cfg['N_GPS'], latitude=cfg['E_GPS'], method='nearest')
                df = newds.to_dataframe()
                # clean if different versions
                nan_rows = df.isna().any(axis=1)
                df_cleaned = df[~nan_rows]
                x = df_cleaned.to_csv(f'{filename}.csv') 
                os.remove(f"{filename}.{ext}")
            GREEN = "\033[92m"
            RESET = "\033[0m"  # Resets the color to default
            print(f"{GREEN}Downloaded: {filename}.{cfg['output'].lower()}\n\n{RESET}")
        except Exception as e:
            RED = "\033[91m"
            RESET = "\033[0m"  # Resets the color to default
            print(f"{RED}Error occurred during download: {e}\n\n{RESET}")

            # Optionally, handle specific exceptions like HTTPError, TimeoutError, etc.
            return  # Skip to next iteration
    else:
        GREEN = "\033[92m"
        RESET = "\033[0m"  # Resets the color to default
        print(f"{GREEN}File already exists: {filename}.{cfg['output'].lower()}\n\n{RESET}")

def display_api_request(cfg, date_range, data_type):
    cds_api_dataset, request = format_cdsapi_request(cfg, date_range, data_type)
    filename = generate_filename(cfg, date_range, data_type)

    print("Request Details:")
    print(f"Dataset: {cds_api_dataset}")
    print(f"Request Parameters: {request}")
    print(f"Intended Filename: {filename}")

def dl(cfg, date_range):
        for dt_range in date_range:
            if cfg["single level parameters"]:
                 display_api_request(cfg, dt_range, 'sl')
                 download_from_api(cfg, dt_range, 'sl')
            if cfg["pressure level parameters"]:
                 display_api_request(cfg, dt_range, 'pl')
                 download_from_api(cfg, dt_range, 'pl')
            if cfg['output'].lower()=="epw":
                processor = EpwConvertor(
                    file_sl=generate_filename(cfg, dt_range, 'sl')+".csv",
                    file_pl=generate_filename(cfg, dt_range, 'pl')+".csv",
                    src=cfg['src'],
                    output=generate_filename(cfg, dt_range, 'sl')+".epw",
                    start_date = convert_date_format(dt_range[0]).replace('-', ''),
                    end_date = convert_date_format(dt_range[1]).replace('-', '')
                )
                processor.convert()
                os.remove(generate_filename(cfg, dt_range, 'sl')+".csv")
                os.remove(generate_filename(cfg, dt_range, 'pl')+".csv")


def display(cfg, date_range):
        for dt_range in date_range:
            if cfg["single level parameters"]:
                 display_api_request(cfg, dt_range, 'sl')
            if cfg["pressure level parameters"]:
                 display_api_request(cfg, dt_range, 'pl')
