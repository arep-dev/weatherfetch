import platform
import datetime
import pandas as pd
from pvlib import  irradiance, solarposition

def get_mouse_wheel_move_units(event):
    """
    Determines the number of units to move the canvas based on a mouse wheel event.

    Args:
        event: The mouse wheel event.

    Returns:
        int: The number of units to move.
    """
    system = platform.system()
    if system == "Windows":
        return int(-1 * (event.delta / 40))
    elif system == "Linux":
        return -1 if event.num == 4 else 1
    elif system == "Darwin":  # macOS
        return int(-1 * event.delta)

def format_time(hour, minute):
    """
    Formats the given hour and minute into a time string.

    Args:
        hour (int): The hour part of the time.
        minute (int): The minute part of the time.

    Returns:
        str: Formatted time string in HH:MM format.
    """
    return f"{hour:02d}:{minute:02d}"


def convert_to_datetime(date_str):
    """
    Converts a date string from the 'dd/mm/yyyy' format to a datetime object.

    @param date_str The date string to be converted.
    @return A datetime object representing the given date.

    Example:
        convert_to_datetime("31/12/2020") returns a datetime object for December 31, 2020.

    Note:
        This function assumes that the input string is in the 'dd/mm/yyyy' format. An exception
        will be raised if the format does not match.
    """
    return datetime.datetime.strptime(date_str, '%d/%m/%Y')

def rename_columns_case_insensitive(df, col_dict):
    """Rename columns in a DataFrame in a case-insensitive manner."""
    new_columns = {}
    lower_case_dict = {key.lower(): value for key, value in col_dict.items()}
    for col in df.columns:
        col_lower = col.lower()
        if col_lower in lower_case_dict:
            new_columns[col] = lower_case_dict[col_lower]
    return df.rename(columns=new_columns)

def round_floats(df):
    for col in df.columns:
        if df[col].dtype == 'float':
            df[col] = df[col].round(1)
    return df


def compute_solar_zenith_angle(lat, lon, times):
    """
    Calculate the solar zenith angle for given latitude(s), longitude(s), and time(s).
    """
    # Create a DataFrame for the combination of inputs
    input_df = pd.DataFrame({
        'latitude': lat,
        'longitude': lon,
        'time': pd.to_datetime(times)
    })

    # Localize times to UTC if not timezone-aware
    if input_df['time'].dt.tz is None:
        input_df['time'] = input_df['time'].dt.tz_localize('UTC')

    # Calculate solar position for each set of values
    solar_positions = solarposition.get_solarposition(input_df['time'], input_df['latitude'], input_df['longitude'])

    # Check for invalid (e.g., nighttime) values and set them to NaN
    solar_positions.loc[solar_positions['elevation'] <= 0, 'apparent_zenith'] = float('nan')
    solar_positions.loc[solar_positions['elevation'] >= 85, 'apparent_zenith'] = float(85)
    # Return the solar zenith angle
    return solar_positions['apparent_zenith'].to_list()


def compute_extra_radiation(times):
    return irradiance.get_extra_radiation(pd.to_datetime(times)).to_list()
