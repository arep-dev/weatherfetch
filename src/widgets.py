import tkinter as tk
from tkinter import ttk
from utilities import get_mouse_wheel_move_units

class VerticalNotebook(ttk.Frame):
    def __init__(self, master=None, tab_side='left', **kw):
        super().__init__(master, **kw)
        self.tab_side = tab_side
        self.tabs = {}
        self.buttons = []
        self._create_widgets()
        self._create_style()
        self.current_tab_id = None
        
    def _create_widgets(self):
        self.button_frame = ttk.Frame(self)
        self.button_frame.pack(side=self.tab_side, fill='y', padx=0, pady=0)
        self.content_frame = ttk.Frame(self)
        self.content_frame.pack(side='left' if self.tab_side == 'right' else 'right',
                                expand=True, fill='both', padx=2, pady=0)

    def _create_style(self):
        style = ttk.Style(self)
        style.configure('LeftAlign.TButton', anchor='w')
        style.configure('TButton', background='lightgray')
        style.map('TButton', background=[('!selected', 'darkgray')])


    def add(self, child, text):
        tab_id = len(self.tabs)
        self.tabs[tab_id] = child
        child.pack(in_=self.content_frame, expand=True, fill='both')
        child.pack_forget()

        btn = ttk.Button(self.button_frame, text=text, 
                         command=lambda: self._raise_tab(tab_id), 
                         style='LeftAlign.TButton')
        btn.pack(fill='x', padx=0, pady=0)
        self.buttons.append(btn)

        if self.current_tab_id is None:
            self._raise_tab(tab_id)

    def _raise_tab(self, tab_id):
        if self.current_tab_id is not None:
            self.buttons[self.current_tab_id].state(['!selected'])
        for tab in self.tabs.values():
            tab.pack_forget()
        self.tabs[tab_id].pack(expand=True, fill='both')
        self.current_tab_id = tab_id
        self.buttons[tab_id].state(['selected'])

    def get_current_tab_name(self):
        """Returns the name of the currently selected tab."""
        if self.current_tab_id is not None:
            return self.buttons[self.current_tab_id].cget('text')
        return None  # Return None if no tab is selected or if current_tab_id is invalid

    def clear_tabs(self):
        """Remove all tabs and their content."""
        # Destroy all buttons
        for btn in self.buttons:
            btn.destroy()
        self.buttons.clear()

        # Destroy all tab frames
        for tab in self.tabs.values():
            tab.destroy()
        self.tabs.clear()

        # Reset the current tab ID
        self.current_tab_id = None


class DataPopup(tk.Toplevel):
    def __init__(self, parent, data_dict, source, output, title="Select Data"):
        super().__init__(parent)
        self.title(title)
        self.data_dict = data_dict
        self.source = source
        self.output = output
        self._create_widgets()
        self._populate_data_categories()
        self.protocol("WM_DELETE_WINDOW", self.on_close)
        self._init_tooltip()

    def _create_widgets(self):
        """Initializes and places the combobox and canvas with scrollbar."""
        self.category_var = tk.StringVar(self)
        self.category_combobox = tk.ttk.Combobox(self, textvariable=self.category_var, state='readonly')
        self.category_combobox.bind("<<ComboboxSelected>>", self._update_displayed_data)
        self.category_combobox.pack(fill='x', padx=5, pady=5)

        # Create a canvas and a scrollbar
        self.canvas = tk.Canvas(self,width=650, height=392)
        scrollbar = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.scrollable_frame = tk.Frame(self.canvas)

        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: self.canvas.configure(
                scrollregion=self.canvas.bbox("all")
            )
        )

        self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")
        self.canvas.configure(yscrollcommand=scrollbar.set)

        self.bind_mousewheel_scrolling()

        # Pack the canvas and scrollbar
        self.canvas.pack(side="left", fill="both", expand=True)
        scrollbar.pack(side="right", fill="y")

        # Create a notebook for displaying content
        self.notebook = VerticalNotebook(self.scrollable_frame)
        self.notebook.pack(fill='both', expand=True)

    def _populate_data_categories(self):
        """Populates the combobox with data categories and sets the default selection."""
        self.category_combobox['values'] = list(self.data_dict[self.source].keys())
        self.category_combobox.set(next(iter(self.data_dict[self.source]), None))  # Default to the first category if available
        self._update_displayed_data()

    def _update_displayed_data(self, event=None):
        """
        Updates checkboxes based on the selected category. Clears existing checkboxes and creates new ones.
        """
        # for widget in self.scrollable_frame.winfo_children():
        #  widget.destroy()
        self.notebook.clear_tabs()

        category = self.category_var.get()

        # Create tabs with padded titles
        for tab in self.data_dict[self.source][category].keys():
            tab_frame = ttk.Frame(self.notebook)
            self.notebook.add(tab_frame, text=tab)
            for item, item_info in self.data_dict[self.source][category][tab].items():
                self._create_item_row(item, item_info, tab_frame)
            row_frame = tk.Frame(tab_frame)
            row_frame.pack(fill='x', expand=False)
            # Create a frame for the buttons
            row_frame = tk.Frame(tab_frame)
            row_frame.pack(fill='x', expand=False)

            # Create and pack buttons
            b_allsel = tk.Button(row_frame, text='all', font=("TkDefaultFont", 8),command=self._select_all_checkboxes)
            b_allsel.pack(side="left", anchor="w")

            b_allunsel = tk.Button(row_frame, text='none', font=("TkDefaultFont", 8),command=self._unselect_all_checkboxes)
            b_allunsel.pack(side="left", anchor="w")

            b_reset = tk.Button(row_frame, text='reset', font=("TkDefaultFont", 8), command=self._reset_all_checkboxes)
            b_reset.pack(side="left", anchor="w")

        self.notebook.pack()


    def _create_item_row(self, item, item_info, obj):
        """Creates a row in the frame for each data item."""
        # Create a frame for each row

        row_frame = tk.Frame(obj)
        row_frame.pack(fill='x', expand=False)

        # Checkbox for the item
        var = tk.IntVar(value=item_info["is_loaded"])
        cb = tk.Checkbutton(row_frame, text=item, variable=var,
                            command=lambda item=item, var=var: self._update_selected_data(item, var))
        cb.pack(side="left", anchor="w")

        # Store the checkbox and its variable in the tab's frame for later reference
        if not hasattr(obj, 'checkboxes'):
            obj.checkboxes = {}  # Initialize if not already present
        obj.checkboxes[item] = var

        # Info icon
        info_icon = ttk.Label(row_frame, text='\u24D8', cursor="hand2")
        info_icon.pack(side="left", padx=(5, 20))

        # Tooltip for info icon
        tooltip = tk.Toplevel(info_icon, bg="white", borderwidth=1)
        tooltip.withdraw()

        # Configure the label for the tooltip with word wrapping, small font, black text on white background
        label = tk.Label(tooltip, text=item_info["info"], bg="white", fg="black", wraplength=250,
                    justify='left', font=("TkDefaultFont", 8))       
        label.pack()

        # Bind tooltip to info icon
        info_icon.bind("<Enter>", lambda event, item_info=item_info["info"]: self._show_tooltip(event, item_info))
        info_icon.bind("<Leave>", self._hide_tooltip)

    def _init_tooltip(self):
        """Initializes a single tooltip window for the class."""
        self.tooltip = tk.Toplevel(self, bg="white", borderwidth=1)
        self.tooltip.withdraw()
        self.tooltip.overrideredirect(True)  # Remove window decorations
        self.tooltip_label = tk.Label(self.tooltip, bg="white", fg="black", wraplength=250, justify='left', font=("TkDefaultFont", 8))
        self.tooltip_label.pack()

    def _show_tooltip(self, event, text):
        """Shows the tooltip with the given text near the info icon."""
        self.tooltip_label.config(text=text)
        x = event.x_root + 20
        y = event.y_root + 20
        self.tooltip.geometry(f"+{x}+{y}")
        self.tooltip.deiconify()

    def _hide_tooltip(self, event):
        """Hides the tooltip."""
        self.tooltip.withdraw()

    def _select_all_checkboxes(self):
        # Access the current tab using the notebook's current_tab_id
        if self.notebook.current_tab_id is not None:
            current_tab_frame = self.notebook.tabs[self.notebook.current_tab_id]
            if hasattr(current_tab_frame, 'checkboxes'):
                for item, var in current_tab_frame.checkboxes.items():
                    var.set(1)  # Set all checkboxes to checked
                    self._update_selected_data(item, var)

    def _unselect_all_checkboxes(self):
        # Access the current tab using the notebook's current_tab_id
        if self.notebook.current_tab_id is not None:
            current_tab_frame = self.notebook.tabs[self.notebook.current_tab_id]
            if hasattr(current_tab_frame, 'checkboxes'):
                for item, var in current_tab_frame.checkboxes.items():
                    var.set(0)  # Set all checkboxes to checked
                    self._update_selected_data(item, var)

    def _reset_all_checkboxes(self):
        # Access the current tab using the notebook's current_tab_id
        if self.notebook.current_tab_id is not None:
            current_tab_frame = self.notebook.tabs[self.notebook.current_tab_id]
            if hasattr(current_tab_frame, 'checkboxes'):
                for item, var in current_tab_frame.checkboxes.items():
                    if self.output=="WRF":
                        var.set(self.data_dict[self.source][self.category_var.get()][self.notebook.get_current_tab_name()][item]["is_WRF"])
                    elif self.output=="EPW":
                        var.set(self.data_dict[self.source][self.category_var.get()][self.notebook.get_current_tab_name()][item]["is_EPW"])
                    else:
                        var.set(0)
                    self._update_selected_data(item, var)

    def _update_selected_data(self, item, var):
        """Updates the selected_items_dict based on checkbox interactions."""
        self.data_dict[self.source][self.category_var.get()][self.notebook.get_current_tab_name()][item]["is_loaded"] = bool(var.get())


    def bind_mousewheel_scrolling(self):
        """Enables scrolling via the mouse wheel across different platforms."""
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)
        self.canvas.bind_all("<Button-4>", self._on_mousewheel)
        self.canvas.bind_all("<Button-5>", self._on_mousewheel)

    def _on_mousewheel(self, event):
        """Handles mouse wheel scroll events."""
        self.canvas.yview_scroll(get_mouse_wheel_move_units(event), "units")

    def on_close(self):
        """Handles the closing event of the popup, updating the selected levels."""
        self.master.selected_data = self.data_dict
        self.destroy()

class LevelPopup(tk.Toplevel):
    """
    LevelPopup creates a popup window for selecting levels from a given list.

    Attributes:
        parent (widget): The parent widget.
        items (list): List of items to be displayed as selectable options.
        selected_items (list): List of pre-selected items.
        title (str): Window title.
    """
    def __init__(self, parent, data_dict, source, output, title="Select Level"):
        super().__init__(parent)
        self.title(title)
        self.selected_levels = data_dict
        self.source = source
        self.output = output
        self._create_widgets()
        self.protocol("WM_DELETE_WINDOW", self.on_close)
        self._init_tooltip()


    def _create_widgets(self):
        """Initializes and places the canvas with checkboxes for each item."""
        # Create a canvas and a scrollbar
        self.canvas = tk.Canvas(self)
        scrollbar = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.scrollable_frame = tk.Frame(self.canvas)

        self.scrollable_frame.bind(
            "<Configure>",
            lambda e: self.canvas.configure(
                scrollregion=self.canvas.bbox("all")
            )
        )

        self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")
        self.canvas.configure(yscrollcommand=scrollbar.set)
        self.canvas.pack(side="left", fill="both", expand=True)
        scrollbar.pack(side="right", fill="y")
        self.bind_mousewheel_scrolling()

        for item, item_info in self.selected_levels[self.source][self.output].items():

            row_frame = tk.Frame(self.scrollable_frame)
            row_frame.pack(fill='x', expand=False)


            var = tk.IntVar(value=item_info["is_loaded"])
            cb = tk.Checkbutton(row_frame, text=item+str(" mb"), variable=var,
                                command=lambda item=item, var=var: self._update_selected_data(item, var))
            cb.pack(side="left", anchor="w")

            # Info icon
            info_icon = ttk.Label(row_frame, text='\u24D8', cursor="hand2")
            info_icon.pack(side="left", padx=(5, 20))

            # Tooltip for info icon
            tooltip = tk.Toplevel(info_icon, bg="white", borderwidth=1)
            tooltip.withdraw()

            label = tk.Label(tooltip, text=item_info["info"], bg="white", fg="black", wraplength=200, justify='left', font=("TkDefaultFont", 8))
            label.pack()

            # Bind tooltip to info icon

            info_icon.bind("<Enter>", lambda event, item_info=item_info["info"]: self._show_tooltip(event, item_info))
            info_icon.bind("<Leave>", self._hide_tooltip)


    def _update_selected_data(self, item, var):
        """Updates the selected_items_dict based on checkbox interactions."""
        self.selected_levels[self.source][self.output][item]["is_loaded"] = bool(var.get())

    def bind_mousewheel_scrolling(self):
        """Enables scrolling via the mouse wheel across different platforms."""
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)
        self.canvas.bind_all("<Button-4>", self._on_mousewheel)
        self.canvas.bind_all("<Button-5>", self._on_mousewheel)

    def _init_tooltip(self):
        """Initializes a single tooltip window for the class."""
        self.tooltip = tk.Toplevel(self, bg="white", borderwidth=1)
        self.tooltip.withdraw()
        self.tooltip.overrideredirect(True)  # Remove window decorations
        self.tooltip_label = tk.Label(self.tooltip, bg="white", fg="black", wraplength=200, justify='left', font=("TkDefaultFont", 8))
        self.tooltip_label.pack()

    def _show_tooltip(self, event, text):
        """Shows the tooltip with the given text near the info icon."""
        self.tooltip_label.config(text=text)
        x = event.x_root + 20
        y = event.y_root + 20
        self.tooltip.geometry(f"+{x}+{y}")
        self.tooltip.deiconify()

    def _hide_tooltip(self, event):
        """Hides the tooltip."""
        self.tooltip.withdraw()

    def _on_mousewheel(self, event):
        """Handles mouse wheel scroll events."""
        self.canvas.yview_scroll(get_mouse_wheel_move_units(event), "units")

    def on_close(self):
        """Handles the closing event of the popup, updating the selected levels."""
        self.master.selected_levels = self.selected_levels
        self.destroy()

class TimeSelector(tk.Frame):
    """
    TimeSelector is a widget for selecting a specific time (hours and minutes).

    Attributes:
        parent (widget): The parent widget.
        hour (str): Initial hour value.
        minute (str): Initial minute value.
    """
    def __init__(self, parent, hour='00', minute='00', *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.hourstr = tk.StringVar(self, hour)
        self.minstr = tk.StringVar(self, minute)
        self.last_value = ""
        self._create_widgets()
        self._setup_bindings()

    def _create_widgets(self):
        """Initializes and places the hour and minute spinboxes."""
        self.hour = tk.Spinbox(self, from_=0, to=23, wrap=True, textvariable=self.hourstr, width=2, state="readonly")
        self.min = tk.Spinbox(self, from_=0, to=59, wrap=True, textvariable=self.minstr, width=2, state="readonly")
        self.hour.grid(row=0, column=0)
        self.min.grid(row=0, column=1)

    def _setup_bindings(self):
        """Sets up bindings for the spinbox widgets."""
        self.minstr.trace("w", self._trace_var)

    def _trace_var(self, *args):
        """Trace callback to handle changes in the minute spinbox."""
        if self.last_value == "59" and self.minstr.get() == "0":
            hour_value = int(self.hourstr.get())
            self.hourstr.set(f"{(hour_value + 1) % 24:02d}")
        self.last_value = self.minstr.get()

class TimeStepSelector(tk.Frame):
    """
    TimeStepSelector is a widget for selecting a time step in hours.

    Attributes:
        parent (widget): The parent widget.
        hour (str): Initial hour value for the time step.
    """
    def __init__(self, parent, hour='01', *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        self.hourstr = tk.StringVar(self, hour)
        self._create_widgets()

    def _create_widgets(self):
        """Initializes and places the hour spinbox for the time step."""
        self.dt = tk.Spinbox(self, from_=1, to=24, wrap=True, textvariable=self.hourstr, width=2, state="readonly")
        self.dt.grid(row=0, column=0)

    def set_time_step(self, hour):
        """Sets the time step to the specified hour."""
        self.hourstr.set(hour)
