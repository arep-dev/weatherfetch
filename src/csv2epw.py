# -*- coding: utf-8 -*-
"""
Script to process weather data and generate EPW file
Created on Fri Dec 22 09:55:39 2023
@author: sauvagea
"""

import pandas as pd
import numpy as np
from geopy.geocoders import Photon
import pytz
from timezonefinder import TimezoneFinder
import numpy as np
import requests
from datetime import datetime, timedelta
from utilities import rename_columns_case_insensitive, round_floats, compute_solar_zenith_angle, compute_extra_radiation

class EpwConvertor:
    """Process weather data for EPW file generation."""

    def __init__(self, file_sl, file_pl, src, output, start_date, end_date):
        self.file_sl = file_sl
        self.file_pl = file_pl
        self.src = src
        self.output = output
        self.TREF = 273.15
        self.df = None
        self.LAT = None
        self.LON = None
        self.city = None
        self.timezone_str = None
        self.utc_offset = None
        self.z = None
        self.monthly_mean_skt = None
        self.start_date = pd.to_datetime(start_date)
        self.end_date = pd.to_datetime(end_date)
        
        self.convert()
        
    def convert(self):
        self._set_db_correspondance()
        self._load_and_merge_data()
        self._preprocess_data()
        self._determine_location_and_timezone()
        self._determine_altitude()
        self._compute_additional_parameters()
        self._generate_epw_content()
        self._save_to_file()

    def _set_db_correspondance(self):
        if self.src == "ERA5 - Reanalysis":
            self.db_correspondance = {
                "T2M": "Dry Bulb Temperature",
                "D2M": "Dew Point Temperature",
                "STL1": "Surface Temperature",
                "MSL": "Atmospheric Station Pressure",
                "TISR": "Extraterrestrial Horizontal Radiation",
                "STRDC": "Horizontal Infrared Radiation Intensity",
                "SSRD": "Global Horizontal Radiation",
                "FDIR": "Direct Horizontal Radiation",
                "10U": "10m Wind",
                "10V": "10m Wind",
                "TCC": "Total Sky Cover",
                "TCWV": "Precipitable Water",
                "SD": "Snow Depth",
                "FAL": "Albedo",
                "CRR": "Convective rain rate",
                "LSRR": "Large scale rain rate",
                "R": "Relative Humidity"
            }

    def _load_and_merge_data(self):
        """Load and merge data from single level and pressure level files."""
        df_sl = pd.read_csv(self.file_sl)
        df_pl = pd.read_csv(self.file_pl)
        self.df = pd.merge(df_sl, df_pl, on=['time', 'latitude', 'longitude'])
        self.df = rename_columns_case_insensitive(self.df, self.db_correspondance)

    def _preprocess_data(self):
        """Preprocess data for EPW file format."""

        self.df['time'] = pd.to_datetime(self.df['time'])
        self.df['time'] = self.df['time'] + pd.Timedelta(hours=-1)
        self.df = self.df[(self.df['time'] >= self.start_date) & (self.df['time'] < self.end_date+timedelta(days=1))]
        
        self.df['YEAR'] = self.df['time'].dt.year.astype(str).str.zfill(4)     # Year
        self.df['MONTH'] = self.df['time'].dt.month.astype(str).str.zfill(2)    # Month
        self.df['DAY'] = self.df['time'].dt.day.astype(str).str.zfill(2)      # Day
        self.df['HOUR'] = (self.df['time'].dt.hour+1).astype(str).str.zfill(2)    # Hour
        self.df['MINUTE'] = self.df['time'].dt.minute.astype(str).str.zfill(2)   # Minut

    def _determine_location_and_timezone(self):
        """Determine location and timezone from latitude and longitude."""
        self.LAT = self.df.latitude.iloc[0]
        self.LON = self.df.longitude.iloc[0]
        geolocator = Photon(user_agent="measurements")
        location = geolocator.reverse((self.LAT, self.LON), exactly_one=True)
        place_name = location.address if location else "Unknown Location"
        address_parts = [part.strip() for part in place_name.split(',')]
        self.city = address_parts[-3] if len(address_parts) >= 3 else "Unknown City"

        tf = TimezoneFinder()
        timezone_str = tf.timezone_at(lng=self.LON, lat=self.LAT)
        timezone = pytz.timezone(timezone_str)
        self.utc_offset = timezone.utcoffset(datetime.now().replace(tzinfo=None)).total_seconds() / 3600

    def _determine_altitude(self):
        """Determine altitude from latitude and longitude."""
        api_url = f"https://api.opentopodata.org/v1/eudem25m?locations={self.LAT},{self.LON}"
        response = requests.get(api_url)
        elevation_data = response.json()
        self.z = elevation_data['results'][0]['elevation'] if elevation_data['results'] else None

    def _compute_additional_parameters(self):
        """Compute additional parameters required for EPW file."""
        epsilon = (0.787 + 0.764 * np.log(self.df['Dew Point Temperature'] / 273)) * (
            1 + 0.0224 * self.df['Total Sky Cover'] - 0.0035 * self.df['Total Sky Cover'] ** 2 + 0.00028 * self.df['Total Sky Cover'] ** 3)
        # Calculate missing data
        self.df['zenith']=compute_solar_zenith_angle(self.df.latitude.tolist(), self.df.longitude.tolist(), self.df['time'].to_list())
    
        self.df["Global Horizontal Radiation"] = self.df["Global Horizontal Radiation"]/3600.0
        self.df["Direct Horizontal Radiation"] = self.df["Direct Horizontal Radiation"]/3600.0
        self.df["Horizontal Infrared Radiation Intensity"] = self.df["Horizontal Infrared Radiation Intensity"]/3600.0
        self.df["Extraterrestrial Direct Normal Radiation"]=compute_extra_radiation(self.df['time'].to_list())
        self.df["Extraterrestrial Horizontal Radiation"]=self.df["Extraterrestrial Horizontal Radiation"]/3600.0
        self.df["Diffuse Horizontal Radiation"]=self.df["Global Horizontal Radiation"]-self.df["Direct Horizontal Radiation"]
        self.df['Direct Normal Radiation'] = 9999
        self.df['Direct Normal Radiation'] = np.where(
            np.isnan(self.df['zenith']), 
            0,  # Set to 0 if zenith is NaN
            self.df['Direct Horizontal Radiation'] / np.cos(np.radians(self.df['zenith']))
        )
        self.df['Extraterrestrial Direct Normal Radiation'] = np.where(
            np.isnan(self.df['zenith']), 
            0,  # Set to 0 if zenith is NaN
            self.df['Extraterrestrial Direct Normal Radiation']
        )
        self.df["Global Horizontal Illuminance"]=110*self.df["Global Horizontal Radiation"]
        self.df["Direct Normal Illuminance"]=105*self.df["Direct Normal Radiation"]
        self.df["Diffuse Horizontal Illuminance"]=119*self.df["Diffuse Horizontal Radiation"]
        self.df["Zenith Luminance"]=9999
        self.df["Opaque Sky Cover"]=99
        self.df["Total Sky Cover"]= round(self.df["Total Sky Cover"]*10.0)
        self.df["Visibility"]=9999
        self.df["Ceiling Height"]=99999
        self.df["Present Weather Observation"]=9
        self.df["Present Weather Codes"]=999999999 
        self.df["Aerosol Optical Depth"]=999


        self.df.loc[self.df['Snow Depth'] < 0.0001, 'Snow Depth'] = 0
        self.df['Liquid Precipitation Depth'] =  self.df['Convective rain rate'] + self.df['Large scale rain rate']
        self.df.loc[self.df['Liquid Precipitation Depth'] < 0, 'Liquid Precipitation Depth'] = 0
        self.df['Snow Depth']*=100
        self.df['Liquid Precipitation Depth']*=3600
        self.df['Liquid Precipitation Quantity']=1
        df_daily = self.df.resample('D', on='time')['Snow Depth'].sum()
        snowing_days = df_daily.diff().fillna(0) > 0.0001
        days_since_snow = snowing_days.groupby((snowing_days != snowing_days.shift()).cumsum()).cumcount()
        days_since_snow = days_since_snow.reindex(self.df['time'].dt.date, method='ffill').fillna(0)
        days_since_snow_df = pd.DataFrame({'Days Since Last Snowfall': days_since_snow.values}, index=self.df['time'])
        self.df=self.df.merge(days_since_snow_df, left_on='time', right_index=True)
        
        # update temperature
        self.df["Dry Bulb Temperature"]-=self.TREF
        self.df["Dew Point Temperature"]-=self.TREF
        self.df["Surface Temperature"]-=self.TREF
        
        # extract monthly mean skin temperature
        self.monthly_mean_skt = self.df['Surface Temperature'].groupby(self.df['MONTH']).mean()
        self.monthly_mean_skt = self.monthly_mean_skt.tolist()
        self.monthly_mean_skt = [round(value, 1) for value in self.monthly_mean_skt]

        
        # Calculate wind speed
        self.df['Wind Speed'] = np.sqrt(self.df['u10']**2 + self.df['v10']**2)
        self.df['Wind Direction'] = (180 + (180 / np.pi) * np.arctan2(self.df['v10'], self.df['u10'])) % 360
        
        self.df["Data Source and Uncertainty Flags"]="*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?*?"
        


    def _generate_epw_content(self):
        """Generate content for EPW file."""
        self.df = self.df.drop(columns=['time', "Surface Temperature", "latitude", "longitude", "u10", "v10", "Large scale rain rate", "Convective rain rate", 'Direct Horizontal Radiation', 'zenith'])
        self.df = self.df[['YEAR', 'MONTH', 'DAY', 'HOUR', 'MINUTE', 'Data Source and Uncertainty Flags',
                 'Dry Bulb Temperature', 'Dew Point Temperature', 'Relative Humidity',
                 'Atmospheric Station Pressure', 'Extraterrestrial Horizontal Radiation',
                 'Extraterrestrial Direct Normal Radiation', 'Horizontal Infrared Radiation Intensity',
                 'Global Horizontal Radiation', 'Direct Normal Radiation', 'Diffuse Horizontal Radiation',
                 'Global Horizontal Illuminance', 'Direct Normal Illuminance', 'Diffuse Horizontal Illuminance',
                 'Zenith Luminance', 'Wind Direction', 'Wind Speed', 'Total Sky Cover',
                 'Opaque Sky Cover', 'Visibility', 'Ceiling Height', 'Present Weather Observation',
                 'Present Weather Codes', 'Precipitable Water', 'Aerosol Optical Depth',
                 'Snow Depth', 'Days Since Last Snowfall', 'Albedo', 'Liquid Precipitation Depth',
                 'Liquid Precipitation Quantity']]

        self.df=round_floats(self.df)

        self.epw_content = [
            f"LOCATION,{self.city},-,-,WF1,999,{self.LAT},{self.LON},{self.utc_offset},{self.z}",
            "DESIGN CONDITIONS,0",
            "TYPICAL/EXTREME PERIODS,0",
            f"GROUND TEMPERATURES,1,1.0,,,,{','.join(map(str, self.monthly_mean_skt))}",
            "HOLIDAYS/DAYLIGHT SAVINGS,No,0,0,0",
            "COMMENTS 1, WeatherFetch AREP v1.0",
            "COMMENTS 2,",
            "DATA PERIODS,1,1,Data,Sunday,1/1,12/31"
        ]


    def _save_to_file(self):
        """Save generated EPW content to a file."""
        df_string = self.df.to_csv(sep=',', header=False, index=False, lineterminator='\n')
        self.full_epw_content = "\n".join(self.epw_content)+ "\n"  + df_string

        with open(self.output, "w") as file:
            file.write(self.full_epw_content)
