from preproc_client import Preprocessor
from dl_client import Downloader
import customtkinter
from constants import *
import tkinter as tk

class Selector(customtkinter.CTk):
    def __init__(self, *args, **kwargs):
        """
        Initialize the main application window and its widgets.
        """
        super().__init__(*args, **kwargs)
        self.title("WheatherFetch Tool Selector")
        self.geometry(f"{226}x{80}")
        self.minsize(225, 80)
        self.maxsize(275, 90)
        self.tool = "WeatherFetch Preprocessor"
        self.initialize_widget()

    def start(self):
        self.mainloop()

    def initialize_widget(self):
        self.frame = customtkinter.CTkFrame(master=self, corner_radius=0)
        self.frame.grid(row=0, column=0, padx=0, pady=0, sticky="nsew")
        self.frame.grid_rowconfigure(0, weight=0, pad=0)
        self.frame.grid_rowconfigure(1, weight=0, pad=0)
        self.frame.grid_rowconfigure(2, weight=1, pad=0)
        self.combobox = tk.ttk.Combobox(self.frame, values=["WeatherFetch Preprocessor", "Client Download"], state='readonly', width=25)
        self.combobox.grid(row=0, column=0, columnspan=1, padx=5, pady=(10, 5), sticky="ew")
        self.combobox.set("WeatherFetch Preprocessor")
        self.button = tk.Button(self.frame, text="Ok", command=self._run)
        self.button.grid(row=1, column=0, columnspan=1, padx=5, pady=(5, 10), sticky="ew")
        # Bind resize event
        self.bind("<Configure>", self._on_resize)
        self.combobox.bind("<<ComboboxSelected>>", self._on_value_change)

    def _on_resize(self, event):
        # Calculate the new width for the left frame and buttons
        width = self.winfo_width()  # Get the current width of the main window
        frame_width = width  # 25% of total width, but not less than 150
        # Update the width of the frame and buttons
        self.frame.configure(width=int(frame_width), height = int(self.winfo_width()) )
        self.frame.grid_propagate(False)  # Prevent the frame from resizing to fit its content

    def _on_value_change(self,event):
        self.focus_set()
        event.widget.selection_clear()
        self.tool = self.combobox.get()

    def _after_cancel_all(self):
        after_info = self.tk.call('after', 'info')
        # Check if the response is a tuple and convert it to a string
        if isinstance(after_info, tuple):
            after_info = " ".join(after_info)
        for job in after_info.split():
            self.after_cancel(job)

    def _run(self):
        self._after_cancel_all()
        self.quit()  # This will exit the mainloop of the Selector
        self.destroy()  # This will destroy all widgets and clean up resources

        if self.tool == "WeatherFetch Preprocessor":
            app = Preprocessor()
        else :
            app = Downloader()
        app.start()

if __name__ == "__main__":
    app = Selector()
    app.start()
