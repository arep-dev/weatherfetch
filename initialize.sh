#!/bin/bash

# Project Name
PROJECT_NAME="WeatherFetch"

# Create project directory
mkdir $PROJECT_NAME

# Create subdirectories
mkdir -p $PROJECT_NAME/src
mkdir -p $PROJECT_NAME/resources/html
mkdir -p $PROJECT_NAME/resources/css
mkdir -p $PROJECT_NAME/resources/images/icons
mkdir -p $PROJECT_NAME/tests
mkdir -p $PROJECT_NAME/docs

# Create Python files in src
touch $PROJECT_NAME/src/main.py
touch $PROJECT_NAME/src/map_interface.py
touch $PROJECT_NAME/src/data_fetcher.py
touch $PROJECT_NAME/src/gui_elements.py
touch $PROJECT_NAME/src/utils.py

# Create resource files
touch $PROJECT_NAME/resources/html/map.html
touch $PROJECT_NAME/resources/css/style.css

# Create test files
touch $PROJECT_NAME/tests/test_data_fetcher.py
touch $PROJECT_NAME/tests/test_gui_elements.py

# Create documentation and README files
touch $PROJECT_NAME/docs/documentation.md
touch $PROJECT_NAME/README.md

# Create a requirements.txt file
touch $PROJECT_NAME/requirements.txt

echo "WeatherFetch project structure initialized successfully."
