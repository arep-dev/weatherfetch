
# Documentation for WeatherFetch Application

## Table of Contents

1. Overview
2. Module Descriptions
   - constants.py
   - csv2epw.py
   - dl_client.py
   - get_emcwf_era.py
   - main.py
   - preproc_client.py
   - utilities.py
   - widgets.py
3. Detailed Class and Function Descriptions
4. Data Processing for EPW Generation
5. Usage 
6. Future Development and Extensions

## 1. Overview

WeatherFetch is a Python-based application designed for downloading, processing, and managing climatic data. It features a user-friendly GUI and supports various data formats, including EPW. The application is structured into several modules, each handling specific aspects of the workflow.

## 2. Module Descriptions

### constants.py
- **Purpose**: Provides a central repository for all constants used in the application.
- **Features**:
  - **Map Defaults**: Includes default zoom levels, center coordinates, and map style settings.
  - **Time and Date**: Default formats and settings for handling dates and times throughout the application.
  - **Appearance Modes**: Definitions for UI themes, such as light or dark mode preferences.
  - **Data Sources**: URLs and identifiers for various climatic data sources, ensuring easy maintenance and updates.
  - **Output Formats**: Preset configurations for different output formats, aiding in data export and utilization.

### csv2epw.py
- **Purpose**: Central module for converting climatic data from CSV format to EPW format.
- **Key Functions**:
  - **Data Loading**: Reads and combines single-level and pressure-level data files.
  - **Location Processing**: Computes timezone and other location-based parameters from geographic coordinates.
  - **EPW Computations**: Transforms and adjusts data to meet EPW format requirements, including temperature and humidity processing.
  - **File Handling**: Manages the creation and storage of the final EPW files.

### dl_client.py
- **Purpose**: Handles the downloading of climate data from various sources.
- **Functionalities**:
  - **Interval Setting**: Allows users to specify the time range for data downloads.
  - **Download Management**: Manages multiple download threads for efficient data retrieval.
  - **Error Handling**: Robust error and exception handling during the download process.
  - **UI Elements**: Provides real-time download progress updates to users through the GUI.

### get_emcwf_era.py
- **Purpose**: Manages the API requests for ECMWF ERA dataset.
- **Key Tasks**:
  - **Request Preparation**: Formats and structures API requests for data retrieval.
  - **Date Handling**: Parses and formats dates to match API requirements.
  - **Parameter Management**: Ensures correct configuration of request parameters.
  - **Error Management**: Handles potential errors and exceptions during API interaction.

### main.py
- **Role**: Acts as the central hub for initiating and managing the application workflow.
- **Main Responsibilities**:
  - **GUI Initialization**: Sets up the main application window and user interface elements.
  - **Module Coordination**: Facilitates communication and data flow between different modules.
  - **User Input Handling**: Processes user actions and inputs, triggering corresponding processes.
  - **Notification Management**: Displays alerts and messages based on application status and user actions.

### preproc_client.py
- **Purpose**: Specializes in data preprocessing and GUI configuration for data selection.
- **Capabilities**:
  - **Data Filtering**: Offers interfaces for selecting and filtering specific climatic data.
  - **GUI Configurations**: Manages checkboxes, tooltips, and other interactive GUI elements.
  - **Preprocessing Tasks**: Performs necessary data transformations for user-selected formats.
  - **User Feedback**: Provides visual feedback and updates on data processing status.

### utilities.py
- **Role**: Offers a suite of utility functions to support various application processes.
- **Includes**:
  - **File Operations**: Functions for common file interactions like reading, writing, and format conversions.
  - **Data Helpers**: Utilities for manipulating and processing data arrays and structures.
  - **GUI Helpers**: Shared functions for creating and managing GUI components.

### widgets.py
- **Purpose**: Designs and implements custom widgets for an enhanced user experience.
- **Details**:
  - **Custom Components**: Develops unique UI elements tailored to specific application needs.
  - **Integration**: Ensures seamless integration of widgets with the main application GUI.
  - **User Interaction**: Enhances user interaction with intuitive and responsive design elements.


## 3. Detailed Class and Function Descriptions

Each module consists of several classes and functions with specific roles. Here, we describe them in detail.

### constants.py
- This module holds constants used across the application.

### csv2epw.py
- **class EpwConvertor:** A class responsible for converting climate data from CSV format to EPW format.
  - **def __init__:** Initializes the EpwConvertor with necessary parameters.
  - **def convert:** Main method to perform the conversion process.
  - **def _set_db_correspondance:** Sets up database correspondences for data fields.
  - **def _load_and_merge_data:** Loads and merges different data sources into a unified format.
  - **def _preprocess_data:** Preprocesses data to meet EPW file requirements.
  - **def _determine_location_and_timezone:** Determines the location and timezone from given data.
  - **def _determine_altitude:** Calculates altitude based on location data.
  - **def _compute_additional_parameters:** Computes additional parameters required for EPW files.
  - **def _generate_epw_content:** Generates content formatted for EPW files.
  - **def _save_to_file:** Saves the generated content into an EPW file.

### dl_client.py
- **def read_config:** Reads and parses the configuration file.
- **def add_months:** Adds a specified number of months to a given date.
- **def calculate_interval:** Calculates the time interval for data downloading.
- **def generate_date_ranges:** Generates a range of dates for data downloading.
- **def select_config_file:** Allows the user to select a configuration file.
- **class ToolTip:** A class that creates tooltip elements in the GUI.
  - **def __init__:** Initializes a tooltip with specified properties.
  - **def enter:** Handles the mouse enter event on the tooltip.
  - **def leave:** Handles the mouse leave event on the tooltip.
  - **def schedule:** Schedules the display of the tooltip.
  - **def unschedule:** Cancels the scheduled display of the tooltip.
  - **def show:** Shows the tooltip.
  - **def hide:** Hides the tooltip.
- **class Downloader:** A class responsible for handling the downloading of climate data.
  - **def __init__:** Initializes the downloader with necessary parameters.
  - **def start:** Starts the download process.
  - **def initialize_widget:** Initializes the widget components for the downloader.
  - **def _on_resize:** Handles the resize event of the download window.
  - **def _select_config_file:** Allows selection of a configuration file for download settings.
  - **def _select_output_path:** Sets the output path for downloaded data.
  - **def _on_value_change:** Handles changes in download parameter values.
  - **def _run:** Runs the download process.
  - **def _dl:** Performs the actual data download.
  - **def _abort:** Aborts the download process.
  - **def _stop_thread:** Stops the download thread safely.

### get_emcwf_era.py
- **def convert_date_format:** Converts dates into a specific format required by the API.
- **def st_format:** Formats strings in a specific style.
- **def format_cdsapi_request:** Formats the request for the CDS API.
- **def generate_filename:** Generates filenames based on download parameters.
- **def download_from_api:** Handles the downloading of data from the API.
- **def display_api_request:** Displays the API request for user confirmation.
- **def dl:** The main function to trigger the download from the API.
- **def display:** Displays the download interface.

### main.py
- **class Selector:** A class representing the main selection window of the application.
  - **def __init__:** Initializes the main selector window.
  - **def start:** Starts the application.
  - **def initialize_widget:** Initializes widgets in the main window.
  - **def _on_resize:** Handles resizing of the main window.
  - **def _on_value_change:** Handles changes in selected values in the window.
  - **def _after_cancel_all:** Handles the event after all selections are cancelled.
  - **def _run:** Runs the main application process.
  
### preproc_client.py
- **class Preprocessor**: A class responsible for preprocessing and setting up data for download and conversion.
  - **def __init__**: Initializes the Preprocessor with default settings and UI components.
  - **def initialize_widgets**: Sets up the initial GUI widgets for the preprocessor.
  - **def configure_tabs**: Configures tabbed sections in the preprocessor interface for better organization.
  - **def initialize_map_widget**: Initializes the map widget for selecting geographical areas.
  - **def initialize_map_options**: Sets up various options and settings for the map widget.
  - **def initialize_time_tab_widgets**: Prepares the widgets for time range selection.
  - **def initialize_data_tab_widgets**: Sets up the widgets for selecting specific types of climatic data.
  - **def on_closing**: Handles actions to be taken when the preprocessor window is closing.
  - **def on_tab_change**: Manages actions when switching between different tabs in the UI.
  - **def open_data_popup**: Opens a popup window for detailed data selection.
  - **def open_level_popup**: Opens a popup for selecting specific data levels (if applicable).
  - **def on_wrf_checked**: Handles actions when the WRF data format is selected.
  - **def on_epw_checked**: Handles actions when the EPW data format is selected.
  - **def _presel_data**: Pre-selects data based on certain criteria or user preferences.
  - **def update_output_format**: Updates the output format based on user selections.
  - **def reset_data**: Resets data selections to default or initial states.
  - **def validate_time**: Validates the selected time range for data download.
  - **def set_event**: Sets up events related to data selection and processing.
  - **def clear_event**: Clears specific events or selections in the preprocessor.
  - **def search_event**: Handles search events within the preprocessor interface.
  - **def set_marker_event**: Manages events related to setting markers on the map.
  - **def create_marker_at**: Creates a marker at a specified location on the map.
  - **def clear_marker_event**: Clears marker-related events on the map.
  - **def clear_area_event**: Handles the clearing of selected areas on the map.
  - **def change_appearance_mode**: Changes the appearance mode (e.g., theme) of the preprocessor.
  - **def change_map**: Alters the map settings or type within the preprocessor.
  - **def on_closing**: Manages actions to perform when the preprocessor window is closing.
  - **def start**: Initiates the start of the preprocessing operations.
  - **def on_resize**: Manages actions during the resizing of the preprocessor window.
  - **def set_area_event**: Sets up events related to area selection on the map.
  - **def get_current_bounding_box**: Retrieves the current bounding box from the map selection.
  - **def _check_configuration**: Checks and validates the current configuration settings.
  - **def _find_place_name**: Finds and retrieves the name of a place based on map coordinates.
  - **def export_data**: Exports the selected and processed data to the chosen format.
  
### utilities.py
- **def get_mouse_wheel_move_units**: Determines the amount of movement caused by the mouse wheel.
- **def format_time**: Formats time values into a standardized format.
- **def convert_to_datetime**: Converts various time formats into Python datetime objects.
- **def rename_columns_case_insensitive**: Renames dataframe columns in a case-insensitive manner.
- **def round_floats**: Rounds floating point numbers to a specified precision.
- **def compute_solar_zenith_angle**: Calculates the solar zenith angle for given geographic coordinates and time.
- **def compute_extra_radiation**: Computes extra-terrestrial radiation for a given day of the year.

### widgets.py
- **class VerticalNotebook**: A custom notebook widget arranged vertically.
  - **def __init__**: Initializes the VerticalNotebook with specified settings.
  - **def _create_widgets**: Creates and adds widgets to the VerticalNotebook.
  - **def _create_style**: Configures the style and appearance of the notebook.
  - **def add**: Adds new tabs to the VerticalNotebook.
  - **def _raise_tab**: Brings a specified tab to the foreground.
  - **def get_current_tab_name**: Retrieves the name of the currently active tab.
  - **def clear_tabs**: Clears all tabs from the notebook.
- **class DataPopup**: A popup window for data selection and settings.
  - **def __init__**: Constructs the DataPopup with necessary elements.
  - **def _create_widgets**: Sets up the widgets inside the popup.
  - **def _populate_data_categories**: Fills the popup with data category options.
  - **def _update_displayed_data**: Updates the display based on selected data categories.
  - **def _create_item_row**: Creates a row of items (options) in the popup.
  - **def _init_tooltip**: Initializes tooltips for items in the popup.
  - **def _show_tooltip**: Displays a tooltip for an item.
  - **def _hide_tooltip**: Hides the currently displayed tooltip.
  - **def _select_all_checkboxes**: Selects all checkboxes in the popup.
  - **def _unselect_all_checkboxes**: Unselects all checkboxes in the popup.
  - **def _reset_all_checkboxes**: Resets all checkboxes to their default state.
  - **def _update_selected_data**: Updates the data selection based on checkbox states.
  - **def bind_mousewheel_scrolling**: Enables scrolling via mouse wheel in the popup.
  - **def _on_mousewheel**: Handles mouse wheel scroll events.
  - **def on_close**: Manages actions to take when the popup window is closed.
- **class LevelPopup**: Similar to DataPopup but for selecting data levels.
  - [Methods are similar to those in DataPopup, adapted for level selection]
- **class TimeSelector**: A widget for selecting time ranges.
  - **def __init__**: Initializes the TimeSelector with necessary configurations.
  - **def _create_widgets**: Creates the widgets for time selection.
  - **def _setup_bindings**: Sets up bindings for interactive elements in the widget.
  - **def _trace_var**: Tracks changes in the widget's variables.
- **class TimeStepSelector**: A widget for selecting time steps.
  - **def __init__**: Initializes the TimeStepSelector.
  - **def _create_widgets**: Creates widgets for selecting time steps.
  - **def set_time_step**: Sets the time step based on user input.


## 4. Data Processing for EPW Generation

The EPW file generation process in `csv2epw.py` involves several steps:

1. **Data Loading and Merging**: Initial data is loaded from CSV files and merged.
2. **Preprocessing**: Data is preprocessed to fit the EPW format, including computation of additional parameters like altitude and timezone.
3. **EPW-Specific Computations**: Includes calculations of missing data points, temperature adjustments, and wind speed computations.
4. **File Generation**: The final step involves formatting the data into the EPW file structure and saving it.

### Epsilon Calculation
- **Description**: Epsilon is a parameter representing sky emissivity, influenced by dew point temperature and sky cover.
- **Formula**:
  \[ \epsilon = (0.787 + 0.764 \cdot \ln(\frac{\text{Dew Point Temperature}}{273})) \times (1 + 0.0224 \times \text{Total Sky Cover} - 0.0035 \times \text{Total Sky Cover}^2 + 0.00028 \times \text{Total Sky Cover}^3) \]
- **Source**: [EnergyPlus Weather File (EPW) Data Dictionary](https://bigladdersoftware.com/epx/docs/8-3/auxiliary-programs/energyplus-weather-file-epw-data-dictionary.html)

### Solar Zenith Angle Calculation (pvlib)
- **Description**: The solar zenith angle is a critical parameter in solar radiation modeling. It is the angle between the vertical and the line to the sun, at solar noon the solar zenith angle is 0°.
- **Formula**:
  - **Declination (\(\delta\))**: \[ \delta = 23.45 \cdot \sin\left(\frac{360}{365} \cdot (284 + n)\right) \]
  - **Hour Angle (H)**: \[ H = 15 \cdot (LST - 12) \]
  - **Solar Zenith Angle (\( \theta_z \))**: \[ \theta_z = \arccos(\sin(\text{latitude}) \cdot \sin(\delta) + \cos(\text{latitude}) \cdot \cos(\delta) \cdot \cos(H)) \]
  - where \( n \) is the day of the year, \( LST \) is the Local Solar Time.
- **Source**: The calculation is based on standard solar position algorithms, like those implemented in pvlib, a Python library for solar energy modeling.

### Radiation and Illuminance Calculations
- **Description**: Conversion of radiation values from Joules per square meter to Watts per square meter (J/m² to Wh/m²) and estimation of illuminance.
- **Formulas**:
  - **Radiation Conversion**: Divide by 3600 to convert from J/m² to Wh/m².
  - **Illuminance Estimation**:
    - Global Horizontal Illuminance: \[ 110 \times \text{Global Horizontal Radiation} \]
    - Direct Normal Illuminance: \[ 105 \times \text{Direct Normal Radiation} \]
    - Diffuse Horizontal Illuminance: \[ 119 \times \text{Diffuse Horizontal Radiation} \]
- **Source**: The coefficients for illuminance calculations are derived from empirical studies and standard practices in lighting and solar radiation modeling.

### Wind Speed and Direction Calculation
- **Description**: Calculation of wind speed and direction using vector components.
- **Formulas**:
  - **Wind Speed**: \[ \sqrt{\text{u10}^2 + \text{v10}^2} \]
  - **Wind Direction**: \[ (180 + \frac{180}{\pi} \cdot \arctan2(\text{v10}, \text{u10})) \mod 360 \]

### Temperature Adjustments
- **Description**: Adjusting temperature readings relative to a reference temperature.
- **Formula**: Subtracting `TREF` from temperature values for standardization.

### Precipitation and Snow Depth Calculations
- **Description**: Calculating precipitation depth and adjusting snow depth measurements.
- **Formulas**:
  - **Precipitation Depth**: Sum of convective and large-scale rain rates.
  - **Snow Depth Adjustment**: Scaling factor applied for unit conversion.

### Days Since Last Snowfall
- **Description**: Tracking the number of days since the last snowfall, significant in cold climate analysis.
- **Formula**: Difference and grouping of snowfall events to calculate days since last snow.


## 5. Usage 

### Installation
Before running WeatherFetch, ensure you have Python installed on your system. WeatherFetch requires Python 3.x.

1. **Clone the repository or download the source code.**
   ```sh
   git clone https://gitlab.com/arep-dev/weatherfetch.git
   cd WeatherFetch
   ```

2. **Install the required dependencies.**
   A `requirements.txt` file is provided. Install the dependencies using pip:
   ```sh
   pip install -r requirements.txt
   ```

3. **Configure the .cdsapirc file.**
   WeatherFetch requires a `.cdsapirc` file for accessing climate data. Create this file in your home directory with the following content:
   ```plaintext
   url: https://cds.climate.copernicus.eu/api/v2
   key: [your-key-token]
   ```
   Replace `[your-key-token]` with your actual API key token. For information on obtaining a token, visit [CDS API key registration](https://cds.climate.copernicus.eu/api-how-to).

### Running the Application
To run WeatherFetch, execute the following command from the application's root directory:

```sh
sh weatherfetch.sh
```

This script will start the application, opening the first GUI window.

### Using WeatherFetch
Upon launching, you will be greeted with a window where you can select between the Preprocessor and the Client for downloading climatic data.

#### Preprocessor
The Preprocessor is designed to help you specify:

- **Geographic Area**: Choose a specific area either by marking a point or defining a region.
- **Data Selection**: Select the specific climatic data you wish to download.
- **Temporal Period**: Define the time frame for which you want the data.
- **Output Format**: Choose the desired format for the downloaded data, such as WRF or EPW.

#### Client for Downloading Climatic Data
This option allows you to directly download climatic data from available databases, currently limited to ERA.

- **Specify Output Path**: Users can define the path where the downloaded data will be saved.
- **Data Subsampling**: The tool allows you to split the downloaded data into smaller time periods (e.g., years, semesters, months). This is particularly useful for avoiding HTTP errors from the CDS server due to large data requests.


## 6. Future Development and Extensions

As the WeatherFetch application continues to evolve, future enhancements may include support for a broader range of climatic data sources. Currently, the application is limited to utilizing ERA5 hourly data reanalysis, but the architecture is designed to be flexible and adaptable. Future versions could integrate additional databases, providing users with a more diverse and comprehensive selection of climatic data for various applications and analyses.
