import cdsapi
import os
from datetime import datetime, timedelta
import xarray as xr
import pandas as pd
import netCDF4 as nc

filename = "Place_48.86_2.32-sl"
ext = "nc"

ds = xr.open_dataset(f"{filename}.{ext}")
newds=ds.sel(longitude=48.8588897, latitude=2.3200410217200726, method='nearest')
df = newds.to_dataframe()
# clean if different versions
nan_rows = df.isna().any(axis=1)
df_cleaned = df[~nan_rows]
x = df_cleaned.to_csv(f'{filename}.csv') 
os.remove(f"{filename}.{ext}")